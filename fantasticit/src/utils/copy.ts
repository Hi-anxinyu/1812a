import { message } from "antd";
import copy from "copy-to-clipboard";
export function copyText(text: string) {
  copy(text);
  message.success('copy success!');
}