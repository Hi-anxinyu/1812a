export default{
    "menu.article":"문장.",
    "menu.archive":"압축 파일",
    "menu.knowledge":'지식 소책자',
    "menu.msgboard":'메모판',
    "menu.about":'...에 대하 여',
    "menu.language.Chinese":'중국어.',
    "menu.language.Japanese":'일본말',
    "menu.language.English":'영어.',
    "menu.language.Korean":'한국어 공부 해 요.',
}   