export default{
    "menu.article":"文章",
    "menu.archive":"アーカイブ",
    "menu.knowledge":'知識小冊',
    "menu.msgboard":'伝言板',
    "menu.about":'について',
    "menu.language.Chinese":'中国語',
    "menu.language.Japanese":'日本語',
    "menu.language.English":'英語',
    "menu.language.Korean":'韓国語',
}  