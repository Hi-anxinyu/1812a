export default{
    "menu.article":"文章",
    "menu.archive":"归档",
    "menu.knowledge":'知识小册',
    "menu.msgboard":'留言板',
    "menu.about":'关于',
    "menu.language.Chinese":'中文',
    "menu.language.Japanese":'日语',
    "menu.language.English":'英语',
    "menu.language.Korean":'韩语',
}   