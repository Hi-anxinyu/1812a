export default{
    "menu.article":"article",
    "menu.archive":"archive",
    "menu.knowledge":'knowledge',
    "menu.msgboard":'msgboard',
    "menu.about":'about',
    "menu.language.Chinese":'Chinese',
    "menu.language.Japanese":'Japanese',
    "menu.language.English":'English',
    "menu.language.Korean":'Korean',
}  