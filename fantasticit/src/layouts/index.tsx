import React, { useState, useEffect } from 'react';
import Header from '@/components/header';
import Footer from '@/components/footer';
import Nav from '@/components/nav';
import styles from '@/assets/css/layout.less';
import Theme from '@/components/Theme';
import { useSelector, useIntl, setLocale, useDispatch, NavLink, useHistory } from 'umi';
import { IRootState } from '@/types';
import { Spin, Select, Input, Space, message, BackTop } from 'antd';
import { LoadingOutlined, SearchOutlined, CloseOutlined, VerticalAlignTopOutlined} from '@ant-design/icons';
var classNames = require('classnames');
const Layouts: React.FC = (props) => {
    const antIcon = <LoadingOutlined style={{ fontSize: 24, color: 'red' }} spin />
    const [visible, showVisible] = useState(false);
    const [flag, setFlag] = useState(false);
    const [sflag, setSflag] = useState(false);
    const [width, setWidth] = useState(window.innerWidth);
    const intl = useIntl();
    const dispatch = useDispatch();
    const { Search } = Input;
    const history = useHistory();
    const { global, language, searchData } = useSelector((state: IRootState) => {
        return {
            global: state.loading.global,
            language: state.language,
            searchData: state.article.searchData
        }
    });
    function onSearch(val: string) {
        if (val === '') {
            message.error({
                content: '请输入有效值'
            })
            return
        }
        dispatch({
            type: 'article/getSearchData',
            payload: val
        })
    }
    useEffect(() => {
        const handleResize = () => {
            setWidth(window.innerWidth)
        }
        window.addEventListener('resize', handleResize);
        width < 770 ? setFlag(true) : setFlag(false);
        return (() => {
            window.removeEventListener('resize', handleResize)
        })
    }, [width])
    useEffect(() => {
        setLocale(language.locale, false)
    }, [language.locale])
    useEffect(() => {
        const handleKeydown = (e: KeyboardEvent) => {
            if (e.code === 'Escape') {
                setSflag(false);
            }
        }
        document.addEventListener('keydown', handleKeydown)
        return (() => {
            document.removeEventListener('keydown', handleKeydown);
        })
    }, [])
    function changeLanguage(locale: string) {
        dispatch({
            type: 'language/save',
            payload: {
                locale,
            }
        })
    }
    function opSearch() {
        setSflag(sflag => !sflag);
        dispatch({
            type: 'article/save',
            payload: {
                searchData: []
            }
        })
    }
    const menus = [
        {
            text: 'menu.article',
            path: '/'
        },
        {
            text: 'menu.archive',
            path: '/archives'
        },
        {
            text: 'menu.knowledge',
            path: '/knowledge'
        },
        {
            text: 'menu.msgboard',
            path: '/page/msgboard'
        },
        {
            text: 'menu.about',
            path: '/page/about'
        }
    ]
    return <div className={classNames(styles.layout)}>
        {global && <div className={styles.showSpin}><Spin tip='加载中...' style={{ color: 'red' }} indicator={antIcon} ></Spin></div>}
        <Header>
            <div className={styles.layout_header}>
                <div className={classNames(styles.container, 'container')}>
                    <div className={styles.log_box}>
                        <NavLink to="/">
                            <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" alt="" />
                        </NavLink>
                    </div>
                    <Nav navlist={menus} className={styles.headerNav} className1={styles.nav}>
                        <li className={classNames(styles.func_li, styles.func_lang)}>
                            <Select value={language.locale} onChange={(val) => changeLanguage(val)}>
                                {
                                    language.locales.map((item: any) => {
                                        return <Select.Option key={item.value} value={item.value}>
                                            {intl.formatMessage({ id: item.label, defaultMessage: '' })}
                                        </Select.Option>
                                    })
                                }
                            </Select>
                        </li>
                        <li className={styles.func_li}>
                            <Theme></Theme>
                        </li>
                        <li onClick={() => opSearch()}>
                            <SearchOutlined style={{ fontSize: '22px' }} />
                        </li>
                    </Nav>
                    {/* 搜索布局 */}
                    {
                        sflag && <div className={styles.dialog}>
                            <div style={{ transform: 'none' }}>
                                <div className={styles.container1}>
                                    <header>
                                        <span className={styles.title}>文章搜索</span>
                                        <span className={styles.close}>
                                            <CloseOutlined onClick={() => opSearch()} />
                                            <span>esc</span>
                                        </span>
                                    </header>
                                    <section className={styles.search}>
                                        <Search placeholder="input search text"
                                            onSearch={(val) => onSearch(val)}
                                            style={{ width: '100%' }}
                                            size='large'
                                        />
                                    </section>
                                    <section className={styles.search_box}>
                                        <ul>
                                            {
                                                searchData.map(item => {
                                                    return <li
                                                        key={item.id}
                                                        style={{ opacity: 1, height: 48 }}
                                                        onClick={() => { setSflag(sflag => !sflag); history.push(`/article/${item.id}`) }}
                                                    >
                                                        {item.title}
                                                    </li>
                                                })
                                            }
                                        </ul>
                                    </section>
                                </div>
                            </div>
                        </div>
                    }
                    {
                        visible && <Nav navlist={menus} className={styles.headerNav2}>
                            <li className={styles.func_li}>
                                <Select value={language.locale} onChange={(val) => changeLanguage(val)}>
                                    {
                                        language.locales.map((item: any) => {
                                            return <Select.Option key={item.value} value={item.value}>
                                                {intl.formatMessage({ id: item.label, defaultMessage: '' })}
                                            </Select.Option>
                                        })
                                    }
                                </Select>
                            </li>
                            <li>
                                <Theme></Theme>
                            </li>
                            <li className={styles.search} onClick={() => setSflag(sflag => !sflag)}>
                                <SearchOutlined style={{ fontSize: '22px' }} />
                            </li>
                        </Nav>
                    }
                    {
                        flag && <div className={classNames(styles.mobileNav, visible ? styles.active : '')} onClick={() => showVisible(!visible)}>
                            <span className={styles.stick}></span>
                            <span className={styles.stick}></span>
                            <span className={styles.stick}></span>
                        </div>
                    }
                </div>
            </div>
        </Header>
        <main>
            {props.children}
            <BackTop>
                <div style={{
                    height: 35,
                    width: 35,
                    lineHeight: '35px',
                    borderRadius: '50%',
                    backgroundColor: 'rgba(0,0,0,.5)',
                    color: '#fff',
                    textAlign: 'center',
                    fontSize: 18,
                }}><VerticalAlignTopOutlined /></div>
            </BackTop>
        </main>
        <Footer>
            <ul className={styles.foot_list}>
                <li>
                    <a href="/rss">
                        <span role="img" className="anticon">
                            <svg viewBox="0 0 1024 1024" version="1.1" p-id="4788" width="24" height="24">
                                <defs>
                                    <style type="text/css"></style>
                                </defs>
                                <path d="M512 0C230.4 0 0 230.4 0 512s230.4 512 512 512 512-230.4 512-512S793.6 0 512 0z m-182.4 768C288 768 256 736 256 694.4s32-73.6 73.6-73.6 73.6 32 73.6 73.6-32 73.6-73.6 73.6z m185.6 0c0-144-115.2-259.2-259.2-259.2v-80c185.6 0 339.2 150.4 339.2 339.2h-80z m172.8 0c0-240-195.2-432-432-432V256c281.6 0 512 230.4 512 512h-80z" fill="currentColor">
                                </path>
                            </svg>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="/rss">
                        <span role="img" className="anticon">
                            <svg viewBox="0 0 1024 1024" version="1.1" p-id="4788" width="24" height="24">
                                <defs>
                                    <style type="text/css"></style>
                                </defs>
                                <path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z" fill="currentColor">
                                </path>
                            </svg>
                        </span>
                    </a>
                </li>
            </ul>
            <div className={styles.text}><p>Designed by <a href="https://github.com/fantasticit/wipi" target="_blank">Fantasticit</a> . <a href="https://admin.blog.wipi.tech/" target="_blank">后台管理</a></p>
                <p>Copyright © 2021. All Rights Reserved.</p>
                <p>皖ICP备18005737号</p>
            </div>
        </Footer>
    </div>
}

export default Layouts;

