import { IRootState } from "@/types";
import { copyText } from "@/utils/copy";
import hljs from "highlight.js";
import React from "react";
import { useEffect } from "react";
import { useSelector } from "umi";
import './index.less'
const HighLight: React.FC = (props) => {
  // console.log(props);
  const dom = React.createRef<HTMLDivElement>();
  const { level } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    let blocks = dom.current!.querySelectorAll('pre code');
    blocks.forEach(item => {
      hljs.highlightElement(item as HTMLDivElement);
      let btn = document.createElement('button');
      btn.textContent = '复制';
      btn.className = 'copy_btn';
      btn.onclick = () => {
        copyText(item.textContent!);
      }
      item.parentNode?.insertBefore(btn, item);
    })
  }, [props.children]);
  return <div ref={dom} className='markdown'>
    {props.children}
  </div>
}
export default HighLight;