import { NavLink } from 'react-router-dom';
import { INavItem } from '@/types';
import { useIntl } from '@/.umi/core/umiExports';
interface IProps {
    navlist: INavItem[],
    className?: string,
    className1?:string,
}
const nav: React.FC<IProps> = (props) => {
    const intl = useIntl()
    const { navlist } = props
    return (
        <div className={props.className1}>
            <ul className={props.className}>
                {
                    navlist.map((item, index) => {
                        return <li key={item.path}>
                            <NavLink to={item.path} exact>{intl.formatMessage({ id: item.text, defaultMessage: '' })}</NavLink>
                        </li>
                    })
                }
                {props.children}
            </ul>
        </div>
    )
}

export default nav;