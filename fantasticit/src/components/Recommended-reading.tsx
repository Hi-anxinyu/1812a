import { NavLink, useDispatch, useSelector } from 'umi';
import { createRef, useEffect, useState } from 'react';
import { IArticleDetail, IArticleItem, IRootClassify, IRootState, ITocItem } from '@/types';
import moment from 'moment';
import styles from "@/assets/css/Recommended-reading.less"
import React from 'react';
import classnames from 'classnames';
import share from '@/components/Share'
moment.locale('zh-cn')
interface Props {
    classfyList?: IRootClassify[];
    recommend?: IArticleItem[];
    tocItme?: ITocItem[];
    item?: IArticleDetail;
}
const RecommendedReading: React.FC<Props> = (props) => {
    const { recommend, level } = useSelector((state: IRootState) => state.article);
    const { item } = props;
    const box = createRef<HTMLDivElement>();
    const dispatch = useDispatch() 
    function changeTabFloorIndex(index: number, id: string) {
        dispatch({
            type: 'article/changeTabFloor',
            payload: index
        });
        document.getElementById(`${id}`)?.scrollIntoView(); 
        
    }
    
    let floors = document.querySelectorAll('h2, h3');
    let floorItem = document.querySelector('.nav')
    useEffect(() => {
        window.onscroll = function () {
            let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
            let curIndex = 0;
            floors.forEach((item: any, index) => {
                if (item.offsetTop - 90 <= scrollTop) {
                    curIndex = index;
                }
            });
            dispatch({
                type: 'article/changeTabFloor',
                payload: curIndex,
            });
        }
    }, [floors])
    useEffect(() => {
        if (level == 0 && box.current) {
            box.current.scrollTop = 0
          } else if(level !== 0 && box.current){
            // box.current!.scrollTop = ([...box.current!.firstElementChild?.children!][level] as HTMLElement).offsetHeight *level + 10;
            box.current.scrollTop = (floorItem as HTMLElement).offsetHeight * level;
        }
    }, [level])
    return (
        <div className={styles.sticky}>
            {
                props.recommend && <div className={styles.recomment_list}>
                    <div className={styles.recomment_title}>
                        <span>推荐阅读</span>
                    </div>
                    <div className={styles.recomment_content}>
                        <div className={styles.recomment_container}>
                            <ul className={styles.recomment_container_ul}>
                                {
                                    recommend.length > 0 && recommend.slice(0, 6).map((item, index) => {
                                        return <li key={item.id} style={{
                                            opacity: 1,
                                            height: '32px',
                                            transform: 'none'
                                        }}>
                                            <NavLink to={`/article/${item.id}`}>
                                                <span>{item.title}</span>  ·  <span><time>{moment(item.publishAt).fromNow()}</time></span>
                                            </NavLink>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            }
            {
                props.classfyList && <div className={styles.archives_classify}>
                    <div className={styles.archivers_title}>
                        <span>文章分类</span>
                    </div>
                    <ul>
                        {
                            props.classfyList.length > 0 && props.classfyList.map(item => {
                                return <li key={item.id} style={{ opacity: 1, height: '37px', transform: 'none', }}>
                                    <a href={`/category/${item.value}`}>
                                        <span>{item.label}</span>
                                        <span>共计 {item.articleCount} 篇文章</span>
                                    </a>
                                </li>
                            })
                        }
                    </ul>
                </div>
            }
            {
                props.tocItme && <div className={styles.cateLog}>
                    <header>目录</header>
                    <main>
                        <div ref={box}>
                            <div className={classnames(styles.cateLog_box, 'tabfloor')} >
                                {
                                    props.tocItme.map((item, index) => {
                                        if (item.level == '2') {
                                            return <div key={index} style={{ opacity: 1, height: '32px' }} className='nav'>
                                                <div className={classnames(styles.toc_item, level == index ? styles.toc_item_bg : null)}
                                                    style={{ paddingLeft: '12px', cursor: 'pointer' }}
                                                    onClick={() => changeTabFloorIndex(index, item.id)}
                                                >{item.text}</div>
                                            </div>
                                        } else {
                                            return <div key={index} style={{ opacity: 1, height: '32px' }} className='nav'>
                                                <div
                                                    className={classnames(styles.toc_item_level, level == index ? styles.toc_item_bg : null)}
                                                    style={{ paddingLeft: '15px', cursor: 'pointer' }}
                                                    onClick={() => changeTabFloorIndex(index, item.id)}
                                                >{item.text}</div>
                                            </div>
                                        }
                                    })
                                }
                            </div>
                        </div>
                    </main>

                </div>
            }

        </div>
    )
}

export default RecommendedReading;

