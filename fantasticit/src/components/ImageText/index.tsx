import React from 'react';
import styles from './index.less';
import { IArticleItem } from '@/types';
import { Divider } from 'antd';
import { EyeOutlined, HeartOutlined, ShareAltOutlined } from '@ant-design/icons';
import monment from 'moment';
import classNames from 'classnames';
import { useEffect } from 'react';
import Share from '@/components/Share';
monment.locale("ZH-CN")
interface IProps {
    data: IArticleItem[],
}
const ImageText: React.FC<IProps> = (props) => {
    const { data} = props;
    //安新宇的白的统计***********
    function handleClickShare(e:React.MouseEvent,item:IArticleItem){
        window._hmt.push(['_trackEvent', '分享', '点击分享', 'id', item.id]);
        e.stopPropagation();
        Share(item)
    }
    //安新宇的白的统计***********
    return (
        <div className={styles.tagdata}>
            <main>
                <div>
                    <div className={styles.databox}>
                        {
                            data.map(item => {
                                return <div className={styles.item} key={item.id}>
                                    <div style={{ opacity: 1 }}>
                                        <div onClick={()=>history.push(`/article/${item.id}`)}>
                                            <header>
                                                <div className={styles.hl}>{item.title}</div>
                                                <div className={styles.hr}>
                                                    <Divider type='vertical'></Divider>
                                                    <span>超过{monment(item.publishAt).fromNow()}</span>
                                                    {
                                                        item.category?.label&&<Divider type='vertical'></Divider>
                                                    }
                                                    {
                                                        item.category?.label&&<span>{item.category?.label}</span>
                                                    }
                                                </div>
                                            </header>
                                            <main>
                                                {
                                                    item.cover&&<div className={styles.ml}>
                                                      <img src={item.cover} alt="" />
                                                     </div>
                                                }
                                                
                                                <div className={styles.mr}>
                                                    <div className={styles.mr_des}>
                                                        {item.summary}
                                                    </div>
                                                    <div className={styles.mr_func}>
                                                        <span>
                                                            <HeartOutlined />
                                                            <span className={styles.mr_func_like}>{item.likes}</span>
                                                        </span>
                                                        <span className={styles.mr_func_D}>.</span>
                                                        <span>
                                                            <EyeOutlined />
                                                            <span>{item.views}</span>
                                                        </span>
                                                        <span className={styles.mr_func_D}>.</span>
                                                        <span className='spanBtn' onClick={(e)=>handleClickShare(e,item)}>
                                                            <ShareAltOutlined />
                                                            <span>分享</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            })
                        }
                    </div>

                </div>
            </main>
        </div>
    )
}

export default ImageText