import { Carousel, Divider } from 'antd';
import { NavLink, useDispatch, useSelector } from 'umi';
import InfiniteScroll from 'react-infinite-scroll-component';
import RecommendedReading from '@/components/Recommended-reading';
import Tag from '@/components/tag';
import Header from '@/components/archivesheader';
import { articleNav } from '@/assets/data/index';
import styles from '@/assets/css/article.less';
import { IArticleItem } from '@/types/article'
import { IRootState } from '@/types';
import ImageText from './ImageText';
import moment from 'moment';
moment.locale('zh-cn')
const classNames = require('classnames');
interface IProps {
    count: number,
    text: string,
    list: IArticleItem[],
    ismarquee: boolean,
    pullupLoader(): void
}
const main: React.FC<IProps> = (props) => {
    const { count, list, pullupLoader, ismarquee, text } = props;
    const { recommend } = useSelector((state: IRootState) => state.article);
    const imgList = recommend.slice(4, 6);
    return (
        <div className='container'>
            <div className={styles.content}>
                <div className={styles.wraper}>
                    <section className={styles.left}>
                        {
                            ismarquee ? <div className={classNames(styles.img_box, styles.img_box1)}    >
                                <div className={styles.img_wraper}>
                                    <Carousel autoplay className={styles.ant_carousel}>
                                        {
                                            imgList.map(item => {
                                                return <div className={styles.aaa}>
                                                    <img src={item.cover} alt="" />
                                                    <div className={styles.img_mask}>
                                                        <h2>{item.title}</h2>
                                                        <p>
                                                            <span>{moment(item.publishAt).fromNow()}</span>
                                                            <span>·</span>
                                                            <span>{item.views}次阅读</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </Carousel>
                                </div>
                            </div> : <Header count={count} text={text}></Header>
                        }
                        <div className={styles.article_warper}>
                            <div className={styles.header}>
                                {
                                    articleNav.map((item) => {
                                        return <NavLink to={item.path} exact key={item.path}><span>{item.text}</span></NavLink>
                                    })
                                }
                            </div>
                            <InfiniteScroll
                                hasMore={list.length < count}
                                loader={<h4>Loading...</h4>}
                                dataLength={list.length}
                                next={pullupLoader}
                            >{
                                    <ImageText data={list}></ImageText>
                                }</InfiniteScroll>

                        </div>
                    </section>
                    <aside className={classNames(styles.right, 'aside')}>
                        <div className={styles.sticky}>
                            <RecommendedReading recommend={recommend}> </RecommendedReading>
                            <Tag></Tag>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    )
}

export default main

