import styles from '@/assets/css/layout.less';
var classNames = require('classnames')
const footer:React.FC=(props)=>{
    return (
        <div className={classNames(styles.footer)}>
            {props.children}
        </div>
    )
}

export default footer;