/*
 * @Author: Hi.yu 
 * @Date: 2021-08-18 10:00:22 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-18 10:07:58
 */
import React from 'react';
import style from '@/pages/archives/index.less';
interface IProps{
    count:number,
    text:string,
}
const archivesheader:React.FC<IProps> = (props) => {
    let {count,text} = props;
    return (
        <div className={style.box_left_main_title}>
            <p><span>{text}</span></p>
            <p>共计<span>{count}</span>篇</p>
        </div>
    )
}

export default archivesheader