import { IArticleDetail } from "@/types";
import React, { useEffect, useState } from "react";
import { Modal } from 'antd'
import Qrcode from 'qrcode'
import ReactDOM from "react-dom";
import { genePost } from "@/services";
interface Props {
  item: IArticleDetail;
}
export const Share: React.FC<Props> = ({ item }) => {
  const [qrSrc, setQrSrc] = useState('');
  const [downloadSrc, setDownloadSrc] = useState('');
  const url = `https://blog.wipi.tech/article/${item.id}`;
  useEffect(() => {
    if (Object.keys(item)) {
      Qrcode.toDataURL(url)
        .then((url: string) => {
          setQrSrc(url)
        })
        .catch(err => {
          console.error(err)
        })
    }
  }, [item]);
  useEffect(() => {
    if (Object.keys(item).length && qrSrc) {
      genePost({
        height: 861,
        html: document.querySelector('.ant-modal-content')?.innerHTML!,
        name: item.title,
        pageUrl: `/article/${item.id}`,
        width: 391
      }).then((res: { data: { url: React.SetStateAction<string>; }; }) => {
        setDownloadSrc(res.data.url);
      })
    }
  }, [item, qrSrc]);
  function cancleModal() {
    ReactDOM.unmountComponentAtNode(document.querySelector('#sharing_box')!);
  }
  return <Modal
    visible={true}
    title="分享海报"
    okText="下载"
    onOk={() => window.location.href = downloadSrc}
    onCancel={cancleModal}
  >
    {item.cover && <img src={item.cover} />}
    <div style={{minWidth: '225px', padding: '12px 0px', border: '0px', marginBottom: '0px', color: 'rgba(0, 0, 0, 0.85)', overflow: 'hidden', fontWeight: 600, fontSize: '16px', lineHeight: '22px'}}>{item.title}</div>
    <p>{item.summary}</p>
    <div className='shareimg'>
      <img src={qrSrc} alt="" />
      <div>
        <p>识别二维码查看文章</p>
        <p>原文分享自
          <a href={url}>小楼又清风</a>
        </p>
      </div>
    </div>
  </Modal>
}

export default function share(item: IArticleDetail) {
  let shareDialog = document.querySelector('#sharing_box');
  if (!shareDialog) {
    shareDialog = document.createElement('div');
    shareDialog.id = 'sharing_box';
    document.body.appendChild(shareDialog);
  }
  ReactDOM.render(<Share item={item} />, shareDialog);
}
