import { IRootState } from "@/types/model";
import { useDispatch, useSelector } from "umi"
import { useEffect } from "react"
import styles from "@/assets/css/ArticleClassification.less"

const ArticleClassification: React.FC = () => {
    const dispatch = useDispatch();
    const { articleClassificationList } = useSelector((state: IRootState) => state.knowledge);
    // console.log(articleClassificationList);

    useEffect(() => {
        dispatch({
            type: 'knowledge/getArticleClassification'
        })
    }, [])
    return (
        <div className={styles.ArticleClassification}>
            <div className={styles.b0LQ}>
                <span>文章分类</span>
            </div>
            <ul>
                {
                    articleClassificationList && articleClassificationList.map((item, index) => {
                        return <li>
                            <a href="" onClick={()=>{
                                history.push(`/article/${item.id}`)
                            }}>
                                <span>{item.label}</span>
                                <span>共计{item.articleCount}篇文章</span>
                            </a>
                        </li>

                    })
                }
            </ul>
        </div>
    )
}

export default ArticleClassification;