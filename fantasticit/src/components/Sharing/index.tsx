import 'antd/dist/antd.css';
import React, { useEffect, useState } from 'react';
import { Modal } from 'antd';
import ReactDOM from 'react-dom';
import { IArticleItem } from '@/types';
import QRCode from 'qrcode'
import {genePost } from '@/services/module/article';
interface IProps {
    item: Partial<IArticleItem>;
}
export const Share: React.FC<IProps> = ({ item }) => {
    const [qrSrc, setQrSrc] = useState('');
    const [downloadSrc, setDownloadSrc] = useState('');
    function cancelClick() {
        ReactDOM.unmountComponentAtNode(document.querySelector('#share-dialog')!);
    }
    const url = `https://blog.wipi.tech/article/${item.id}`;
    useEffect(() => {
        if (Object.keys(item)) {
            QRCode.toDataURL(url)
                .then((url: string) => {
                    setQrSrc(url)
                })
                .catch(err => {
                    console.error(err)
                })
        }
    }, [item]);
    // 生成海报
    useEffect(() => {
        if (Object.keys(item).length && qrSrc) {
            genePost({
                height: 861,
                html: document.querySelector('.ant-modal-content')?.innerHTML!,
                name: item.title!,
                pageUrl: `/article/${item.id}`,
                width: 391
            }).then(res => {
                setDownloadSrc(res.data.url);
            })
        }
    }, [item, qrSrc]);
    return <Modal
        visible={true}
        title="分享海报"
        okText="下载"
        cancelText="取消"
        onOk={() => window.location.href = downloadSrc}
        onCancel={cancelClick}
    >
        <div className="body" style={{ display: 'flex', justifyContent: "center", overflowX: "hidden", flexDirection: "column" }}>
            {item.cover && <img src={item.cover} style={{ width: "100%", borderRadius: "2px" }} />}
            <div style={{ minWidth: "225px", padding: " 12px 0px", color: " rgba(0, 0, 0, 0.85)", overflow: "hidden", fontWeight: "bold", fontSize: "16px", lineHeight: " 22px" }}>{item.title}</div>
            <p style={{ display: "-webkit-box", maxWidth: "100%", padding: " 0px 0px 12px", color: "rgba(0, 0, 0, 0.65)", fontSize: "14px" }}>{item.summary}</p>
            <div style={{ position: "relative", height: "80px" }}>
                <div style={{ position: "absolute", left: "0px", width: "80px", height: "80px", padding: "0px", }}>
                    <img src={qrSrc} alt="" style={{ width: "80px", height: "80px" }} />
                </div>
                <div style={{ position: "absolute", left: "80px", padding: "8px 16px", width: "295px", height: "80px", }}>
                    <p style={{ position: "absolute", top: "0px", width: "100%", color: "rgba(0, 0, 0, 0.85)" }}>识别二维码查看文章</p>
                    <p style={{ position: "absolute", bottom: "0px", color: " rgba(0, 0, 0, 0.65)", fontSize: "0.9em", }}>原文分享自
                    <a href={url} style={{ color: "rgb(255, 0, 100)" }}>小楼又清风</a>
                    </p>
                </div>
            </div>
        </div>
    </Modal>
}

export default function share(item: Partial<IArticleItem>) {
    let shareDialog = document.querySelector('#share-dialog');
    if (!shareDialog) {
        shareDialog = document.createElement('div');
        shareDialog.id = 'share-dialog';
        document.body.appendChild(shareDialog);
    }
    ReactDOM.render(<Share item={item} />, shareDialog);
}
