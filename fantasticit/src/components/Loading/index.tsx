import { Spin } from "antd"

const Loading = () => {
    return <div className="showSpin"><Spin tip='加载中...' style={{ color: 'red' }} ></Spin></div>
}
export default Loading