import React from 'react'
import {Spin} from 'antd'
import { LoadingOutlined} from '@ant-design/icons';
import styles from '@/assets/css/layout.less';
 function index() {
    const antIcon = <LoadingOutlined style={{ fontSize: 24, color: 'red' }} spin />
    return (
        <div className={styles.showSpin}><Spin tip='加载中...' style={{ color: 'red' }} indicator={antIcon}></Spin></div>
    )
} 
export default index