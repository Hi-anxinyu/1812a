import styles from '@/assets/css/layout.less';
import '@/global.less';
const classNames = require('classnames')
interface IProps{
    className?:string
}
const header:React.FC<IProps>=(props)=>{
    return (
        <div className={styles.header}>
            {props.children}
        </div>
    )
}
export default header;