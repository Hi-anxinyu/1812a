import React from 'react'
import styles from '../../assets/css/msgboard.less'
import { Form, Input, Button, Checkbox } from 'antd';
interface Props{
 clsLofin:any,
 name:string,
 email:string,
 ChangeVal:any,
 submit:any
}
 const Login:React.FC<Props>=(props)=>{
     const {clsLofin,name,email,ChangeVal,submit} = props;
    return (
        <div className={styles.login}>
            <div className={styles.login_content}>
                <div className={styles.login_content_title}>
                    <h3>请设置你的信息</h3>
                    <p onClick={()=>clsLofin()}>x</p>
                </div>
                <div className={styles.login_content_inp}>
                    <p><span>* 名 称:</span> <input type="text" defaultValue={name} onChange={(e)=>ChangeVal(e,'name')}/></p>
                    <p><span>* 邮 箱:</span> <input type="text" defaultValue={email} onChange={(e)=>ChangeVal(e,'email')}/></p>
                    <p><button onClick={()=>clsLofin()}>取消</button><button onClick={()=>submit()}>设置</button></p>
                </div>
            </div>
        </div>
    )
}
export default Login