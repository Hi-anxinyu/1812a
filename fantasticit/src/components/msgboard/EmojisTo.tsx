import React from 'react'
import {emojis} from './EmojisFace'
import styles from '../../assets/css/comment.less'
const classnames = require('classnames')

interface Props{
    handleBtnss(e: React.MouseEvent<HTMLSpanElement, MouseEvent>,item:string):void
}

const EmojisTo:React.FC<Props>=(props) => {
        const {handleBtnss}=props
    return (
        <div className={classnames(styles.emojis )}>
             <ul>
                {
                    emojis.map((item,index)=>{
                        return <li onClick={(e)=>handleBtnss(e,item)} key={index}>{item}</li>
                    })
                }
            </ul>
        </div>
    )
}
export default EmojisTo;