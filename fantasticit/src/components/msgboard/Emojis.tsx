import React from 'react'
import {emojis} from './EmojisFace'
import styles from '../../assets/css/comment.less'
const classnames = require('classnames')

interface Props{
    handleBtns(e: React.MouseEvent<HTMLSpanElement, MouseEvent>,item:string):void
}

const Emojis:React.FC<Props>=(props) => {
        const {handleBtns}=props
    return (
        <div className={classnames(styles.emojis ,styles.emojis_hide)}>
             <ul>
                {
                    emojis.map((item,index)=>{
                        return <li onClick={(e)=>handleBtns(e,item)} key={index}>{item}</li>
                    })
                }
            </ul>
        </div>
    )
}
export default Emojis;