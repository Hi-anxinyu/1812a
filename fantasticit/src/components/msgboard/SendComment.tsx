import { ICommentLists } from '@/types'
import moment from 'moment'
import React, { LegacyRef, useState } from 'react'
import styles from '../../assets/css/comment.less'
import Mask from './Mask'
import { Pagination } from 'antd';
import EmojisTo from './EmojisTo'
moment.locale('zh-cn');
const classnames = require('classnames')
interface Props {
    setLogin: any,
    handlerValue: any,
    handlerBtn: any,
    onShowSizeChange: any
    setState: any,
    handlerButton: any,
    id: string,
    commentLists: ICommentLists[],
    total: number,
    colors: Array<string>,
    dom: HTMLDivElement,
    flags:boolean,
    setFace(e:React.MouseEvent<HTMLSpanElement, MouseEvent>,index:number,index2:number):void
    handleFlag():void,
    handleBtns(e:React.MouseEvent<HTMLSpanElement, MouseEvent>,item:string):void,
    val:string,
    handleBtnss(e: React.MouseEvent<HTMLSpanElement, MouseEvent>,item:string):void
    clickDom(e: React.MouseEvent<HTMLSpanElement, MouseEvent>):void
}
const SendComment: React.FC<Props> = (props) => {
    const {
        setLogin,
        handlerValue,
        handlerBtn,
        dom,
        colors,
        setState,
        handlerButton,
        commentLists,
        total,
        onShowSizeChange,
        id,
        flags,
        handleFlag,
        setFace,
        handleBtns,
        val,
        handleBtnss,
        clickDom
    } = props

    
    return (
        <div className={styles.msgboard_banner} onClick={(e)=>clickDom(e)}>
            <div className={styles.msgboard_banner_padding}>
                <div className={classnames(styles.msgboard_banner_title, 'remark')}><span>评论</span></div>
                <div className={classnames(styles.msgboard_banner_inp)}>
                    <textarea name="" id="" placeholder="请输入评论内容（支持 Markdown）"  onClick={setLogin} onChange={handlerValue} ></textarea>
                    <footer>    
                        <p onClick={handleFlag}>
                            <span>
                            <svg viewBox="0 0 1024 1024  ">
                                <path d='M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z'></path>
                            </svg>
                            </span>
                            <span>表情</span>
                        </p>
                        <button onClick={handlerBtn}>发布</button>
                    </footer>
                </div>
                {flags?<EmojisTo handleBtnss={handleBtnss}/>:''}
                <ul className={styles.msgboard_banner_comment} ref={dom as unknown as LegacyRef<HTMLUListElement>}>
                    {commentLists && commentLists.map((item, index) => {
                        return <li key={index} className={styles.msgboard_banner_comment_item}>
                            <div className={styles.msgboard_banner_comment_item_comment}>
                                <div className={styles.msgboard_banner_comment_title}>
                                    <span style={{ backgroundColor: colors[index] }} className={styles.msgboard_banner_comment_item_two_icon}>{item.name.slice(0, 1)}</span>
                                    <span>{item.name}</span>
                                </div>
                                <div className={styles.msgboard_banner_comment_main}>
                                    <p dangerouslySetInnerHTML={{ __html: item.content }}></p>
                                    <p className={styles.msgboard_banner_comment_main_content}>{item.userAgent} ·  {moment(item.createAt).fromNow()}
                                        <span onClick={(e) => setState(e, index, 0)} className={styles.msgboard_banner_comment_main_content_icon} >
                                            <svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path></svg>回复</span>
                                    </p>
                                </div>
                                <div className={classnames(styles.msgboard_banner_comment_mask, styles.msgboard_banner_comment_mask_hidde)}>
                                    <Mask
                                        val={val}
                                        handleBtns={handleBtns}
                                        setFace={(e)=>setFace(e,index,0)}
                                        flags={flags}
                                        handlerValue={(e:React.ChangeEvent<HTMLTextAreaElement>)=>handlerValue(e)}
                                        handlerButton={()=>handlerButton(item)}
                                        setState={(e:React.MouseEvent<HTMLSpanElement>)=>setState(e,index,0)}
                                        item={item}
                                        index={index}
                                        setLogin={()=>setLogin()}
                                    /></div>

                            </div>
                            {item.children !== null ? item.children.map((item2, index2) => {
                                return <div key={index2} className={styles.msgboard_banner_comment_item_comment_two}>
                                    <div className={styles.msgboard_banner_comment_title_two}>

                                        <span style={{ display: 'block' }}><span style={{ fontWeight: 700 }}><span style={{ backgroundColor: colors[index] }} className={styles.msgboard_banner_comment_item_two_icon}>{item2.name.slice(0, 1)}</span>{item2.name}</span>&ensp;回复&ensp;<span style={{ fontWeight: 700 }}>{item.name}</span></span>
                                    </div>
                                    <div className={styles.msgboard_banner_comment_main_two}>
                                        <p className={styles.msgboard_banner_comment_main_two_content} dangerouslySetInnerHTML={{ __html: item2.content }} ></p>
                                        <p className={styles.msgboard_banner_comment_main_content_two}>{item2.userAgent} · {moment(item2.createAt).fromNow()}
                                            <span onClick={(e) => setState(e, index, index2)} className={styles.msgboard_banner_comment_main_content_icon_two}> <svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path></svg>回复</span></p>
                                    </div>
                                    <div className={classnames(styles.msgboard_banner_comment_mask, styles.msgboard_banner_comment_mask_hidde)}><Mask
                                        handleBtns={handleBtns}
                                        setFace={(e)=>setFace(e,index,index2)}
                                        flags={flags}
                                        handlerValue={(e:React.ChangeEvent<HTMLTextAreaElement>) => handlerValue(e)}
                                        setState={(e:React.MouseEvent<HTMLSpanElement, MouseEvent>) => setState(e,index,0)}
                                        handlerButton={() => handlerButton(item)}
                                        item={item2}
                                        index={index2}
                                        setLogin={setLogin}
                                        val={val}
                                    /></div>
                                </div>

                            }) : ''}
                        </li>
                    })}
                </ul>
                <div className={styles.msgboard_banner_comment_page}>
                    <Pagination size="small" total={total} pageSize={6} defaultCurrent={1} onChange={(current) => onShowSizeChange(current)} />
                </div>
            </div>

        </div>
    )
}
export default SendComment;