import {useSelector} from 'umi';
import {IRootState} from '@/types/model';
import styles from '@/assets/css/article.less';
const tag = ()=>{
    const {taglist} = useSelector((state:IRootState)=>state.article);
    return (
        <div className={styles.tag}>
            <div className={styles.tag_title}>
                <span>文章标签</span>
            </div>
            <ul className={styles.tag_list}>
               {
                   taglist.map(item=>{
                       return <li key={item.id}>
                           <a href={`/tag/${item.value}`}>
                               {`${item.label}[${item.articleCount}]`}
                           </a>
                       </li>
                   })
               }
            </ul>
        </div>
    )
}
export default tag