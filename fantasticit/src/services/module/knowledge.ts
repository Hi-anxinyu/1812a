import { request } from 'umi';
// 获取文章列表
//page  1
export function getKnowledgeList(page: number = 1, pageSize = 12, status = 'publish') {
    return request('/api/knowledge', {
        params: {
            page,
            pageSize,
            status,
        }
    })
}
//文章分类
export function getArticleClassification(articleStatus = "publish") {
    return request('/api/category', {
        params: {
            articleStatus
        }
    })
}
//知识页面详情
export function getKnowledgeDetail(id: string) {
    return request(`/api/knowledge/${id}`, {
        params: {
            id
        }
    })
}
//知识页面详情下的详情页面
export function getKnowledgeDetailDetail(sId: string) {
    return request(`/api/knowledge/${sId}`, {
        params: {
            sId
        }
    })
}