import { IGenePost } from '@/types';
import { request } from 'umi';

// 获取文章列表
export function getArticleList(page: number, pageSize = 12, status = 'publish') {
    return request('/api/article', {
        params: {
            page,
            pageSize,
            status,
        }
    })
}

// 获取推荐文章
export function getRecommend(id: string) {
   if (id) {
        return request(`/api/article/recommend?articleId=${id}`, {
        });
   }
   return request(`/api/article/recommend`);
   
}

// 获取tag列表
export function getaglist(articleStatus='publish'){
    return request('/api/tag',{
        params:{
            articleStatus,
        }
    })
}

export function getArticleDetail(id: string) {
    return request(`/api/article/${id}/views`, {
        method: 'POST',
    })
}


export function giveALikes({id, type}: {id: string, type: string}) {
    return request(`/api/article/${id}/likes`, {
        method: 'POST',
        data: {
            type: type,
        }
    });
};

export function genePost(data: IGenePost) {
    return request(`/api/poster`, {
        method: 'POST',
        data,
    })
}

//获取搜索数据
export function getSearchData(keyword:string){
    return request(`/api/search/article`,{
        params:{
            keyword,
        }
    })
}