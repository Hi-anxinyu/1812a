import { request } from "umi";

export function getArchivesList() { // 获取归档数据
  return request('/api/article/archives')
}

export function getClassifyList() { // 获取分类数据
  return request('/api/category?articleStatus=publish')
}