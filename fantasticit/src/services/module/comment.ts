import { request } from "umi";
import {ISendComment} from '../../types/index'
interface Payload{
    page:number;
    id:string
}
//推荐阅读
export function getCommentList(){
    return request('/api/article/recommend')
}
//留言板and关于 头部

export function getAsRegards(id: string){
    return request(`/api/page/${id}/views`,{method:'POST'})
}
//评论列表

export function getCommentListTos({page,id}:Payload,pageSize:number=6){
    
    return request(`/api/comment/host/${id}`,{
        params:{ 
            page: page,
            pageSize:pageSize
        }
    })
}
export function getSendComment({content,name,email,hostId,url,parentCommentId,replyUserEmail,replyUserName}:ISendComment){
    return request('/api/comment',{ 
        method:'POST',
        data:{
            content: content,
            name: name,
            url: url,
            email: email,
            hostId,
            parentCommentId,
            replyUserEmail,
            replyUserName
        }
    })
}


