import { request } from 'umi';

// 获取文章列表
export function getCategoryList(page: number,key:string ,pageSize = 12, status = 'publish') {
    return request(`/api/article/category/${key}`,{
        params: {
            page,
            pageSize,
            status,
        }
    })
}