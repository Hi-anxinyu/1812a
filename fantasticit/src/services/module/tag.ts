import {request} from 'umi';
// https://api.blog.wipi.tech/api/article/tag/git?page=1&pageSize=12&status=publish
export const _getagdata = (page: number, id:string,pageSize = 12, status = 'publish') =>{
    return request(`/api/article/tag/${id}`,{
        params:{
            page,
            pageSize,
            status,
        }
    })
}