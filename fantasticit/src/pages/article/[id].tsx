import HeightLight from "@/components/HeightLight";
import ImageView from "@/components/imageView";
import RecommendedReading from "@/components/Recommended-reading";
import { ICommentList, ICommentLists, IRootState, ITocItem } from "@/types";
import { fomatTime } from "@/utils/seedTime";
import { useState } from "react";
import { useEffect } from "react";
import SendComment from '../../components/msgboard/SendComment'
import { IRouteComponentProps, useDispatch, useSelector } from "umi";
import style from './index.less';
import React from "react";
import { message } from 'antd'
import '../../assets/css/comment.less'
import Login from "@/components/msgboard/Login";
import styleTo from '../../assets/css/comment.less';
import LikeComment from "@/components/LikeComment";
const ArticleDetail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  const id = props.match.params.id;
  const { articleDetail, level } = useSelector((state: IRootState) => state.article);
  const { recommend, tocItme, isLikes } = useSelector((state: IRootState) => state.article);
  const [lisks, setLikes] = useState('');
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: 'article/getArticleDetail',
      payload: id,
    })
  }, [])
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
      payload: id,
    })
  }, [])
  useEffect(() => {
    dispatch({
      type: 'article/giveALikes',
      payload: { id: id, type: 'like' }
    })
  }, [])
  const dom = React.createRef<HTMLDivElement>()
  const [page, setPage] = useState(1)
  const { commentList, commentLists, total } = useSelector((state: ICommentList) => state.comment)
  const [colors, setColors] = useState(['rgb(255, 0, 100)', 'rgb(82, 196, 26)', 'rgb(250, 173, 20)', 'rgb(245, 34, 45)', 'rgb(82, 196, 26)', 'blue'])
  const [result, setResult] = useState(false)
  const [flag, setFlag] = useState(window.localStorage.flag || false)
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [flags, useFlag] = useState(false)
  let [val, setVal] = useState('');
  let [index1, setIndex] = useState(0);
  let [index3, setIndexs] = useState(0);

  //获取推荐阅读列表
  useEffect(() => {
    dispatch({
      type: 'comment/getCommentList',
    })
  }, [])
  //获取评论列表
  useEffect(() => {
    dispatch({
      type: 'comment/getCommentListTos',
      payload: {
        page,
        id: id
      },
    })
  }, [page])
  const Iid = String(new Date().getTime())
  const onShowSizeChange = (current: number) => {
    setPage(current)
  }
  const handlerValue = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    console.log(e.target.value);
    setVal(e.target.value)
  }
  const handlerButton = (item: ICommentLists) => {
    console.log(item);
    dispatch({
      type: 'comment/getSendComment',
      payload: {
        content: val,
        email: email || window.localStorage.email,
        hostId: Iid,
        name: name || window.localStorage.name,
        url: '/page/msgboard',
        parentCommentId: item.id,
        replyUserEmail: item.email,
        replyUserName: item.name
      },
    })
    setTimeout(() => {
      val = ''
    }, 1000)
  }
  const handlerBtn = () => {
    dispatch({
      type: 'comment/getSendComment',
      payload: {
        content: val,
        name: name || window.localStorage.name,
        email: email || window.localStorage.email,
        hostId: Iid,
        url: '/page/msgboard',
      },
    })

  }
  const handleFlag = () => {
    useFlag(!flags)

  }

  const changeLikes = () => {
    dispatch({
      type: 'article/changeLikes',
      payload: id,
    })
  }
  const setState = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number, index2: number) => {
    e.stopPropagation()
    if (e.target == dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild || e.target === dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild?.firstChild || e.target == dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild?.lastChild) {
      dom.current?.children[index].children[0].lastElementChild?.children[0]!.classList.toggle(styleTo.msgboard_banner_inp_hidden)
    } else {
      dom.current?.children[index].children[index2 + 1].lastElementChild!.children[0]!.classList.toggle(styleTo.msgboard_banner_inp_hidden)
    }
  }
  //表情的显示隐藏
  const setFace = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number, index2: number) => {
    e.stopPropagation()
    setIndex(index1 => index)
    setIndexs(index3 => index2)
    console.log(dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.lastElementChild!);
    if (e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild || e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild!.firstElementChild?.lastElementChild || e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild!.firstElementChild?.lastElementChild?.lastElementChild) {

      dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.toggle(styleTo.emojis_hide)
    } else {
      dom.current?.children[index].children[index2 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.toggle(styleTo.emojis_hide)
    }
  }

  //全局事件
  const clickDom = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {

    if (e.target === dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild) {

      dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.remove(styleTo.emojis_hide)
      dom.current?.children[index1].children[index3 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.remove(styleTo.emojis_hide)
    } else {
      dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.add(styleTo.emojis_hide)
      if (dom.current?.children[index1].children[index3 + 1]) {
        dom.current?.children[index1].children[index3 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.add(styleTo.emojis_hide)
        // useFlag(!true)
        return
      }
      if (e.target === dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild) {
        useFlag(true)
        if (flags) {
          useFlag(false)
        }

      } else {

        useFlag(false)
      }
    }
    if (e.target === document.querySelector('footer p') || e.target === document.querySelector('footer span') || e.target == document.querySelectorAll('footer span')[1]) {
      useFlag(true)
    } else {
      useFlag(false)
    }


  }


  const setLogin = () => {
    setResult(true)
  }
  const clsLofin = () => {
    setResult(false)
  }
  const ChangeVal = (e: React.ChangeEvent<HTMLInputElement>, type: string) => {
    if (type == "name") {
      setName(e.target.value)
      console.log(name);

    } else {
      setEmail(e.target.value)
      console.log(email);

    }
    console.log(e.target.value);

  }
  const submit = () => {
    if (name && email) {
      setResult(false)
      message.success('信息保存成功')
      setFlag(true)
      window.localStorage.flag = true
      window.localStorage.name = name
      window.localStorage.email = email
    } else {
      message.error('不能为空')

    }
  }
  // 给表情绑定事件
  const handleBtns = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, item: string) => {
    e.stopPropagation()
    if (e.target.parentNode.parentNode.parentNode.parentNode.children[0].children[0] === dom.current?.children[index1].children[0].lastElementChild?.children[0].firstChild!) {
      dom.current!.children[index1].children[0].lastElementChild!.children[0].firstChild!.value += item
    } else {
      dom.current!.children[index1].children[index3 + 1].lastElementChild!.lastElementChild!.children[0].value += item
    }
    setVal(val => val + item)
  }
  const handleBtnss = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, item: string) => {
    e.stopPropagation()
    dom.current!.parentNode!.children[1].children[0]!.value += item
    setVal(val => val + item)
  }

  return <div className={style.article_detail}>
    <div className='container'>
      <div className={style.article_detail_main}>
        <LikeComment id={id} item={articleDetail} />
        <section className={style.article_detail_left}>
          <article className={style.article_detail_left_article}>
            <ImageView>
              <HeightLight>
                {
                  articleDetail.cover && <img src={articleDetail.cover} alt="" />
                }
                <div className={style.article_detail_title}>
                  <h1 className={style.article_detail_h1}>{articleDetail.title}</h1>
                  <p><span>发布于<time dateTime={fomatTime(articleDetail.publishAt)}>{fomatTime(articleDetail.publishAt)}</time></span><span> • </span><span>{articleDetail.views}</span></p>
                </div>
                <div dangerouslySetInnerHTML={{ __html: articleDetail.html }}></div>
              </HeightLight>
            </ImageView>
            <div className={style.article_detail_session}>
              <div className={style.article_detail_settime}>
                发布于<time dateTime={fomatTime(articleDetail.publishAt)}>{fomatTime(articleDetail.publishAt)}</time>版权信息<a href="">非商用-署名-自由转载</a>
              </div>
            </div>
          </article>
          <SendComment
            clickDom={clickDom}
            handleBtnss={handleBtnss}
            val={val}
            handleBtns={handleBtns}
            handleFlag={handleFlag}
            setLogin={setLogin}
            handlerValue={handlerValue}
            handlerBtn={handlerBtn}
            dom={dom as unknown as HTMLDivElement}
            colors={colors}
            setState={setState}
            handlerButton={handlerButton}
            commentLists={commentLists}
            total={total}
            id={id}
            flags={flags}
            setFace={setFace}
            onShowSizeChange={onShowSizeChange}
          />
          {result && !flag ? <Login
            clsLofin={clsLofin}
            name={name}
            email={email}
            ChangeVal={ChangeVal}
            submit={submit}
          /> : ''}
        </section>
        <aside className={style.article_detail_right}>
          <RecommendedReading recommend={recommend} tocItme={tocItme} item={articleDetail} />
        </aside>
      </div>
    </div>
  </div>
}
export default ArticleDetail;
////////////