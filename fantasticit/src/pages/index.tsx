import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import Main from '@/components/main';
export default function IndexPage() {
  const [page, setsPage] = useState(1)
  const dispatch = useDispatch()
  const { recommend, articlelist, articlecount ,taglist} = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    })
    dispatch({
      type: 'article/getaglist'
    })
  }, [])
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page,
    })
  }, [page])
  useEffect(() => {
      dispatch({
          type: 'article/getRecommend'
      })
  }, [])
  function pullupLoader() {
    setsPage(() => page + 1)
  }
  return (
    <Main list={articlelist} count={articlecount} pullupLoader={pullupLoader} ismarquee={true} text=''> </Main>
  );
}
