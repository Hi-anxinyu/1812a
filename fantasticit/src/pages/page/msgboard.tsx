import styles from '../../assets/css/msgboard.less'
import styleTo from '../../assets/css/comment.less'
import { useDispatch, useSelector } from 'umi'
import { useEffect, useRef, useState } from 'react'
import { ICommentList } from '@/types/model'
import moment from 'moment';
import {  message } from 'antd';
import React from 'react';
import { ICommentLists } from '@/types';
import Login from '@/components/msgboard/Login';
import SendComment from '../../components/msgboard/SendComment'
import ImageText from '../../components/ImageText/index'

moment.locale('zh-cn');
const classnames = require('classnames')

const archives: React.FC = () => {
    const dom = useRef<HTMLInputElement>(null)
    const [page, setPage] = useState(1)
    const dispatch = useDispatch()
    const { commentList, commentLists, total } = useSelector((state: ICommentList) => state.comment)
    const colors = ['rgb(255, 0, 100)', 'rgb(82, 196, 26)', 'rgb(250, 173, 20)', 'rgb(245, 34, 45)', 'rgb(82, 196, 26)', 'blue']
    const [result, setResult] = useState(false)
    const [flag, setFlag] = useState(window.localStorage.flag || false)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [flags, useFlag] = useState(false)
    let hostId: string = '8d8b6492-32e5-44e5-b38b-9a479d1a94bd';
    let [val, setVal] = useState('');
    let [index1, setIndex] = useState(0);
    let [index3, setIndexs] = useState(0);
    //获取推荐阅读列表
    useEffect(() => {
        dispatch({
            type: 'comment/getCommentList',
        })
        
    }, [])
    //获取评论列表
    useEffect(() => {

        dispatch({
            type: 'comment/getCommentListTos',
            payload: {
                page,
                id: hostId
            },
        })

    }, [page])
    const id = String(new Date().getTime())
    const onShowSizeChange = (current: number) => {
        setPage(current)
    }
    const handlerValue = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setVal(e.target.value)
    }
    const handlerButton = (item: ICommentLists) => {
        console.log(item);
        dispatch({
            type: 'comment/getSendComment',
            payload: {
                content: val,
                email: email || window.localStorage.email,
                hostId: id,
                name: name || window.localStorage.name,
                url: '/page/msgboard',
                parentCommentId: item.id,
                replyUserEmail: item.email,
                replyUserName: item.name
            },
        })
        // setVal(val=>'')
    }
    //点击发布
    const handlerBtn = () => {
        dispatch({
            type: 'comment/getSendComment',
            payload: {
                content: val,
                name: name || window.localStorage.name,
                email: email || window.localStorage.email,
                hostId: id,
                url: '/page/msgboard',
            },
        })
        // setVal(val=>'')

    }

    //回复框显示隐藏
    const setState = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number, index2: number) => {
        e.stopPropagation()
        if (e.target === dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild || e.target === dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild?.firstChild || e.target == dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild?.lastChild) {
            dom.current?.children[index].children[0].lastElementChild?.children[0]!.classList.toggle(styleTo.msgboard_banner_inp_hidden)
        } else {
            dom.current?.children[index].children[index2 + 1].lastElementChild!.children[0]!.classList.toggle(styleTo.msgboard_banner_inp_hidden)
        }
    }
    //表情的显示隐藏
    const setFace = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number, index2: number) => {
        e.stopPropagation()
        setIndex(index1 => index)
        setIndexs(index3 => index2)
        console.log(dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild);

        if (e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild || e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild!.firstElementChild?.lastElementChild || e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild!.firstElementChild?.lastElementChild?.lastElementChild) {


            dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.toggle(styleTo.emojis_hide)

        } else {
            dom.current?.children[index].children[index3 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.toggle(styleTo.emojis_hide)
        }
    }
    //全局事件
    const clickDom=(e: React.MouseEvent<HTMLElement, MouseEvent>)=>{
        if(e.target=== dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild ){
    
            dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.remove(styleTo.emojis_hide)
            dom.current?.children[index1].children[index3 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.remove(styleTo.emojis_hide)
        }else{
            dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.add(styleTo.emojis_hide)
            if(dom.current?.children[index1].children[index3 + 1]){
                dom.current?.children[index1].children[index3 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.add(styleTo.emojis_hide)
                // useFlag(!true)
                return
            }
        }
        if(e.target===document.querySelector('footer p')||e.target===document.querySelector('footer span')||e.target==document.querySelectorAll('footer span')[1]){
            useFlag(true)
        }else{
            useFlag(false)
        }
        
    }
    //信息框的显示隐藏
    const setLogin = () => {
        
        setResult(true)
    }
    const clsLofin = () => {
        setResult(false)
    }
    const ChangeVal = (e: React.ChangeEvent<HTMLInputElement>, type: string) => {
        if (type === "name") {
            setName(e.target.value)
        } else {
            setEmail(e.target.value)

        }
    }
    const submit = () => {
        if (name && email) {
            setResult(false)
            message.success('信息保存成功')
            setFlag(true)
            window.localStorage.flag = true
            window.localStorage.name = name
            window.localStorage.email = email
        } else {
            message.error('不能为空')

        }
    }
    //表情
  
    // 给表情绑定事件
    const handleBtns =  (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, item: string) => {
        e.stopPropagation()
        if (e.target.parentNode.parentNode.parentNode.parentNode.children[0].children[0] === dom.current?.children[index1].children[0].lastElementChild?.children[0].firstChild!) {
            dom.current!.children[index1].children[0].lastElementChild!.children[0].firstChild!.value += item
        } else {
            dom.current!.children[index1].children[index3 + 1].lastElementChild!.lastElementChild!.children[0].value +=item
        }
         setVal(val=>val+item)
    }
    const handleBtnss = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>,item: string) => {
        e.stopPropagation()
        dom.current!.parentNode!.children[1].children[0]!.value += item
        setVal(val=>val+item)        
        
    }
    const handleFlag = () => {
        useFlag(!flags)
    }
    return <div className={styles.msgboard}>
                <div className={styles.msgboard_box}>
                    <div className={styles.msgboard_padding}>
                    {result && !flag ? <Login
                        clsLofin={clsLofin}
                        name={name}
                        email={email}
                        ChangeVal={ChangeVal}
                        submit={submit}
                    /> : ''}
                    <div className={styles.msgboard_padding_top_title}>
                        <div className={classnames(styles.msgboard_top)}>
                            <h2>留言板</h2>
                            <h3>「请勿灌水 🤖」</h3>
                        </div>
                    </div>
                </div>

                    <SendComment
                        clickDom={clickDom}
                        val={val}
                        handleBtns={handleBtns}
                        handleBtnss={handleBtnss}
                        handleFlag={handleFlag}
                        flags={flags}
                        setLogin={setLogin}
                        handlerValue={handlerValue}
                        handlerBtn={handlerBtn}
                        dom={dom as unknown as HTMLDivElement}
                        colors={colors}
                        setState={setState}
                        handlerButton={handlerButton}
                        commentLists={commentLists}
                        total={total}
                        id={id}
                        onShowSizeChange={onShowSizeChange}
                        setFace={setFace}
                    />

                    <div className={styles.msgboard_list}>
                        <h2 className={styles.msgboard_list_title}>推荐阅读</h2>
                        {commentList && <ImageText data={commentList} />}
                    </div>
                </div>
           </div>
}
export default archives