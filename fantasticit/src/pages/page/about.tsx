import styles from '../../assets/css/msgboard.less'
import stylesTo from '../../assets/css/comment.less'
import { useDispatch, useSelector } from 'umi'
import { useEffect, useState } from 'react'
import { ICommentList } from '@/types/model'
import moment from 'moment';
import {  message } from 'antd';
import React from 'react';
import { ICommentLists } from '@/types';
import Login from '@/components/msgboard/Login';
import SendComment from '../../components/msgboard/SendComment'
import ImageText from '@/components/ImageText'

moment.locale('zh-cn');

const classnames = require('classnames')
const archives: React.FC = () => {
    const postId = 'a5e81ffe-0ad0-4be9-acca-c0462b1b98a1'
    const dom = React.createRef<HTMLDivElement>()
    const [page, setPage] = useState(1)
    const dispatch = useDispatch()
    const { commentList, commentLists, total, asRegards } = useSelector((state: ICommentList) => state.comment)
    const [colors, setColors] = useState(['rgb(255, 0, 100)', 'rgb(82, 196, 26)', 'rgb(250, 173, 20)', 'rgb(245, 34, 45)', 'rgb(82, 196, 26)', 'blue'])
    const [result, setResult] = useState(false)
    const [flag, setFlag] = useState(window.localStorage.flag || false)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [flags, useFlag] = useState(false)
    let [index1, setIndex] = useState(0);
    let [index3, setIndexs] = useState(0);
    let [val, setVal] = useState('');


    //获取推荐阅读列表
    useEffect(() => {
        dispatch({
            type: 'comment/getCommentList',
        })
    }, [])
    //获取评论列表
    useEffect(() => {

        dispatch({
            type: 'comment/getCommentListTos',
            payload: {
                page,
                id: postId
            },

        })

    }, [page])
    // 获取关于页面
    useEffect(() => {
        dispatch({
            type: 'comment/getAsRegards',
            payload: postId
        })
    }, [])
    const id = String(new Date().getTime())
    const onShowSizeChange = (current: number) => {
        setPage(current)
    }
    const handlerValue = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setVal(e.target.value)
    }
    const handlerButton = (item: ICommentLists) => {
        dispatch({
            type: 'comment/getSendComment',
            payload: {
                content: val,
                email: email || window.localStorage.email,
                hostId: id,
                name: name || window.localStorage.name,
                url: '/page/msgboard',
                parentCommentId: item.id,
                replyUserEmail: item.email,
                replyUserName: item.name
            },
        })

    }
    const handlerBtn = () => {
        dispatch({
            type: 'comment/getSendComment',
            payload: {
                content: val,
                name: name || window.localStorage.name,
                email: email || window.localStorage.email,
                hostId: id,
                url: '/page/msgboard',
            },
        })
    }
    const setState = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number, index2: number) => {
        e.stopPropagation()
        if (e.target == dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild || e.target === dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild?.firstChild || e.target == dom.current?.children[index].children[0].firstChild?.nextSibling?.lastChild?.lastChild?.lastChild) {
            dom.current?.children[index].children[0].lastElementChild?.children[0]!.classList.toggle(stylesTo.msgboard_banner_inp_hidden)
        } else {
            dom.current?.children[index].children[index2 + 1].lastElementChild!.children[0]!.classList.toggle(stylesTo.msgboard_banner_inp_hidden)
        }
    }
    //表情的显示隐藏
    const setFace = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number, index2: number) => {
        //保存下标
        e.stopPropagation()
        setIndex(index1 => index)
        setIndexs(index3 => index2)
        if (e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild || e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild!.firstElementChild?.lastElementChild || e.target === dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild!.firstElementChild?.lastElementChild?.lastElementChild) {
            
            dom.current?.children[index].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.toggle(stylesTo.emojis_hide)

        } else {
            dom.current?.children[index].children[index2 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.toggle(stylesTo.emojis_hide)
        }

    }
    //全局事件
    const clickDom=(e: React.MouseEvent<HTMLElement, MouseEvent>)=>{

            
            if(e.target=== dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild ){
        
                dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.remove(stylesTo.emojis_hide)
                dom.current?.children[index1].children[index3 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.remove(stylesTo.emojis_hide)
            }else{
                dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.lastElementChild!.classList.add(stylesTo.emojis_hide)
                if(dom.current?.children[index1].children[index3 + 1]){
                    dom.current?.children[index1].children[index3 + 1].lastElementChild?.lastElementChild?.lastElementChild!.classList.add(stylesTo.emojis_hide)
                    // useFlag(!true)
                    return
                }
                if(e.target===dom.current?.children[index1].children[0].lastElementChild?.lastElementChild?.children[1].firstElementChild?.lastElementChild){
                    useFlag(true)
                    if(flags){
                        useFlag(false)
                    }
                    
                }else{
                    
                    useFlag(false)
                }
            }
            if(e.target===document.querySelector('footer p')||e.target===document.querySelector('footer span')||e.target==document.querySelectorAll('footer span')[1]){
                useFlag(true)
            }else{
                useFlag(false)
            }
     

    }
    //保存用户名
    const setLogin = () => {
        setResult(true)
    }
    const clsLofin = () => {
        setResult(false)
    }
    const ChangeVal = (e: React.ChangeEvent<HTMLInputElement>, type: string) => {
        if (type == "name") {
            setName(e.target.value)

        } else {
            setEmail(e.target.value)

        }
    }
    const submit = () => {
        if (name && email) {
            setResult(false)
            message.success('信息保存成功')
            setFlag(true)
            window.localStorage.flag = true
            window.localStorage.name = name
            window.localStorage.email = email
        } else {
            message.error('不能为空')

        }
    }
    //
    //表情
    const handleFlag = () => {
        useFlag(!flags)

    }
    // 给表情绑定事件
    const handleBtns = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, item: string) => {
        e.stopPropagation()
        if (e.target.parentNode.parentNode.parentNode.parentNode.children[0].children[0] === dom.current?.children[index1].children[0].lastElementChild?.children[0].firstChild!) {
            dom.current!.children[index1].children[0].lastElementChild!.children[0].firstChild!.value += item
        } else {
            dom.current!.children[index1].children[index3 + 1].lastElementChild!.lastElementChild!.children[0].value += item
        }
        setVal(val => val + item)
    }
    const handleBtnss = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, item: string) => {
        e.stopPropagation()
        dom.current!.parentNode!.children[1].children[0]!.value += item
        setVal(val => val + item)
    }
    return <div className={styles.msgboard}>
                <div className={styles.msgboard_box}>
                    <div className={styles.msgboard_padding}>
                        {result && !flag ? <Login
                            clsLofin={clsLofin}
                            name={name}
                            email={email}
                            ChangeVal={ChangeVal}
                            submit={submit}
                        /> : ''}
                        <div className={styles.msgboard_top_width}>
                            <div className={classnames(styles.msgboard_top)}>
                                <img src={asRegards.cover} alt="" />
                                <h2 dangerouslySetInnerHTML={{ __html: asRegards.content }}></h2>
                            </div>
                        </div>
                    </div>
                    {/* <div className={classnames(styles.msgboard_paddingTo)}> */}

                    <SendComment
                        clickDom={clickDom}
                        val={val}
                        handleBtnss={handleBtnss}
                        handleBtns={handleBtns}
                        handleFlag={handleFlag}
                        setLogin={setLogin}
                        handlerValue={handlerValue}
                        handlerBtn={handlerBtn}
                        dom={dom as unknown as HTMLDivElement}
                        colors={colors}
                        setState={setState}
                        handlerButton={handlerButton}
                        commentLists={commentLists}
                        total={total}
                        id={id}
                        flags={flags}
                        onShowSizeChange={onShowSizeChange}
                        setFace={setFace}
                    />
                    {/* </div> */}
                    <div className={styles.msgboard_list}>
                        <h2 className={styles.msgboard_list_title}>推荐阅读</h2>
                        {commentList && <ImageText data={commentList} />}

                    </div>
                </div>

           </div>
}
export default archives