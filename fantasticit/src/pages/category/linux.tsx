/*
 * @Author: Hi.yu 
 * @Date: 2021-08-17 15:28:14 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-18 08:46:21
 */
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector } from 'umi';
import Main from '@/components/main';
const linux=()=>{
    const [page, setsPage] = useState(1)
    const dispatch = useDispatch()
    const { linuxlist, linuxcount } = useSelector((state: IRootState) => state.linux);
    useEffect(() => {
      dispatch({
        type: 'linux/getCategoryList',
        payload:{
          page,
          key:'linux'
        }
      })
    }, [page])
    function pullupLoader() {
      setsPage(() => page + 1)
    }
    return (
        <Main list={linuxlist} count={linuxcount} pullupLoader={pullupLoader} ismarquee={false} text='linux'></Main>
    )
}

export default linux;