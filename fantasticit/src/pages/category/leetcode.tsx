/*
 * @Author: Hi.yu 
 * @Date: 2021-08-17 15:28:14 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-18 08:53:08
 */
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector } from 'umi';
import Main from '@/components/main';
const leetcode=()=>{
    const [page, setsPage] = useState(1)
    const dispatch = useDispatch()
    const { leetcodelist, leetcodecount } = useSelector((state: IRootState) => state.leetcode);
    useEffect(() => {
      dispatch({
        type: 'leetcode/getCategoryList',
        payload:{
          page,
          key:'leetcode'
        }
      })
    }, [page])
    function pullupLoader() {
      setsPage(() => page + 1)
    }
    return (
        <Main list={leetcodelist} count={leetcodecount} pullupLoader={pullupLoader} ismarquee={false} text='leetcode'></Main>
    )
}

export default leetcode;

