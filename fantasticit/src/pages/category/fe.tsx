/*
 * @Author: Hi.yu 
 * @Date: 2021-08-17 15:28:14 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-18 08:52:20
 */
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector } from 'umi';
import Main from '@/components/main';
const fe=()=>{
    const [page, setsPage] = useState(1)
    const dispatch = useDispatch()
    const { felist, fecount } = useSelector((state: IRootState) => state.fe);
    useEffect(() => {
      dispatch({
        type: 'fe/getCategoryList',
        payload:{
          page,
          key:'fe'
        }
      })
    }, [page])
    function pullupLoader() {
      setsPage(() => page + 1)
    }
    return (
        <Main list={felist} count={fecount} pullupLoader={pullupLoader} ismarquee={false} text='前端'></Main>
    )
}

export default fe;

