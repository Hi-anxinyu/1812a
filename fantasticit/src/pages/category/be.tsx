/*
 * @Author: Hi.yu 
 * @Date: 2021-08-17 15:28:14 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-17 21:23:10
 */
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector } from 'umi';
import Main from '@/components/main';
const be=()=>{
    const [page, setsPage] = useState(1)
    const dispatch = useDispatch()
    const {belist,becount} = useSelector((state: IRootState) => state.be);
    useEffect(() => {
      dispatch({
        type: 'be/getCategoryList',
        payload:{
          page,
          key:'be'
        }
      })
    }, [page])
    function pullupLoader() {
      setsPage(() => page + 1)
    }
    return (
        <Main list={belist} count={becount} pullupLoader={pullupLoader} ismarquee={false} text='后端'></Main>
    )
}

export default be;

