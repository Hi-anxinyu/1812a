/*
 * @Author: Hi.yu 
 * @Date: 2021-08-18 08:32:57 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-18 10:12:56
 */
/*
 * @Author: Hi.yu 
 * @Date: 2021-08-17 15:28:14 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-17 21:23:10
 */
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector } from 'umi';
import Main from '@/components/main';
const reading=()=>{
    const [page, setsPage] = useState(1)
    const dispatch = useDispatch()
    const {readinglist,readingcount} = useSelector((state: IRootState) => state.reading);
    useEffect(() => {
      dispatch({
        type: 'reading/getCategoryList',
        payload:{
          page,
          key:'reading'
        }
      })
    }, [page])
    function pullupLoader() {
      setsPage(() => page + 1)
    }
    return (
        <Main list={readinglist} count={readingcount} pullupLoader={pullupLoader} ismarquee={false} text='阅读'></Main>
    )
}
export default reading;