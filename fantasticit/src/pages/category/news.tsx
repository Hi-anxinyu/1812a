/*
 * @Author: Hi.yu 
 * @Date: 2021-08-18 08:54:43 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-18 08:59:18
 */
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector } from 'umi';
import Main from '@/components/main';
const news=()=>{
    const [page, setsPage] = useState(1)
    const dispatch = useDispatch()
    const { newslist, newscount } = useSelector((state: IRootState) => state.news);
    useEffect(() => {
      dispatch({
        type: 'news/getCategoryList',
        payload:{
          page,
          key:'news'
        }
      })
    }, [page])
    function pullupLoader() {
      setsPage(() => page + 1)
    }
    return (
        <Main list={newslist} count={newscount} pullupLoader={pullupLoader} ismarquee={false} text='要闻'></Main>
    )
}

export default news;