import { useDispatch, useSelector } from "@/.umi/plugin-dva/exports";
import { IRootState, IArchiveRoot } from "@/types/model";
import { useEffect, useState } from "react";
import style from './index.less'
import './index.less';
import { IArchiveItem, _2021, July2 } from "@/types";
import moment from "moment";
import RecommendedReading from "@/components/Recommended-reading";
import Header from "@/components/archivesheader";
import { NavLink } from "umi";
const archives: React.FC = () => {
	const dispatch = useDispatch();
	const { archivesList, classfyList } = useSelector((state: IArchiveRoot) => state.archives);
	const { recommend } = useSelector((state: IRootState) => state.article)
	useEffect(() => {
		dispatch({
			type: 'archives/getArchivesList'
		})
	}, []);
	useEffect(() => {
		dispatch({
			type: 'archives/getClassifyList'
		})
	}, []);
	useEffect(() => {
		dispatch({
			type: 'article/getRecommend'
		})
	}, []);
	return <div className="archivers">
		<div style={{ minHeight: '100%' }}>
			<div className="container">
				<div className={style.box}>
					<section className={style.box_left}>
						<div className={style.box_left_main}>
							<Header text='归档' count={archivesList[2020] && Object.keys(archivesList).reduce((total, item) => total + Object.keys(archivesList[item]).reduce((totals, items) => totals + archivesList[item][items].filter((v: July2, x:number) => v.status === 'publish').length,0),0)}></Header>
							<div>
								{
									archivesList[2020] && Object.keys(archivesList).sort((a, b) => Number(b) - Number(a)).map((item, index) => {
										return <div className={style.box_left_main_content} key={index}>
											<h2 >{item}</h2>
											{
												Object.keys(archivesList[item]).map((items, index) => {
													return <div className={style.content_item} key={index}>
														<h3>{items}</h3>
														{
															archivesList[item][items].map((itemss: July2) => {
																return <ul key={itemss.id}>
																	{
																		<li style={{
																			opacity: 1,
																			height: "48px",
																			transform: 'none',
																		}}>
																			<NavLink to={`/article/${itemss.id}`}>
																				<span className={style.content_item_time}>
																					<time dateTime={moment(itemss.publishAt).format("MM-DD")}>{moment(itemss.publishAt).format("MM-DD")}</time>
																				</span><span className={style.content_item_title}>{itemss.title}</span>
																			</NavLink>
																		</li>
																	}
																</ul>
															})
														}
													</div>
												})
											}
										</div>
									})
								}
							</div>
						</div>
					</section>
					<aside className={style.box_right}>
						<RecommendedReading recommend={recommend} classfyList={classfyList} />
					</aside>
				</div>
			</div>
		</div>
	</div>
}

export default archives