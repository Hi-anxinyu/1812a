import React,{useState,useEffect} from 'react';
import { IArchiveRoot, IRootState } from '@/types';
import { useSelector,useDispatch, NavLink} from 'umi';
import { RouteComponentProps } from 'react-router';
import {Divider} from 'antd';
import styles from '@/assets/css/tag.less';
import monment from 'moment';
import RecommendedReading from '@/components/Recommended-reading';
import { EyeOutlined, HeartOutlined, ShareAltOutlined } from '@ant-design/icons';
import ImageText from '@/components/ImageText';
monment.locale("ZH-CN")
const tag: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
    const dispatch = useDispatch();
    const { match: { params: { id } } } = props;
    const [key,setId] = useState(props.match.params.id);
    const [page,SetPage] = useState(1)
    //调用module 获取数据
    useEffect(()=>{
        dispatch({
            type: 'archives/getClassifyList',
        })
        dispatch({
            type:'article/getaglist'
        })
        dispatch({
            type:'article/getRecommend'
        })
    },[])
    useEffect(()=>{
        dispatch({
            type:'tag/_getagdata',
            payload:{
                page,
                id:key,
            }
        })
    },[key])
    const {taglist,recommend} = useSelector((state: IRootState) => state.article);
    const { classfyList } = useSelector((state: IArchiveRoot) => state.archives);
    const { tagdatalist,tagcount} = useSelector((state:IRootState)=>state.tag);
    const [label,setlabel] = useState(tagdatalist[0]);
    return (
        <div className={styles.wraper}>
            <div className={styles.box} style={{ minHeight: "100vh", background: "var(--bg-body)" }}>
                <div className={styles.container}>
                    <div className={styles.content}>
                        <section className={styles.section}>
                             <div className={styles.tagnum}>
                                <p><span>{id}</span>标签有关的文章</p>
                                <p>共搜索到<span>{tagcount}</span>篇</p>
                             </div>
                             <div className={styles.tag_list}>
                                  <div className={styles.title}>
                                      <span>文章标签</span>
                                  </div>
                                  <ul>
                                      {
                                          taglist.map((item)=>{
                                              return <li key={item.id} className={item.value===id?styles.active:''}>
                                                  <NavLink to={`/tag/${item.value}`} onClick={()=>setId(item.value)}>{`${item.label}[${item.articleCount}]`}</NavLink>
                                              </li>
                                          })
                                      }
                                  </ul>
                             </div>
                             <ImageText data={tagdatalist}></ImageText>     
                        </section>
                        <aside className={styles.aside}>
                             <RecommendedReading  recommend={recommend} classfyList={classfyList}></RecommendedReading>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default tag