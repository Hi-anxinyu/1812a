import { IArchiveRoot, IRootState } from "@/types/model";
import { useEffect } from "react"
import { useDispatch, useSelector } from "umi"
import RecommendedReading from "@/components/Recommended-reading";
import Knowledge from "@/components/knowledge/knowledge";
import styles from '@/components/knowledge/knowledge.less';
import '@/global.less'
const archives: React.FC = () => {
    const dispatch = useDispatch();
    const { knowledgeList } = useSelector((state: IRootState) => state.knowledge);
    const { classfyList } = useSelector((state: IArchiveRoot) => state.archives);
    const { recommend } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        dispatch({
            type: 'knowledge/getKnowledgeList'
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: 'archives/getClassifyList'
        })
    }, []);
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
    }, [])
    return (
        <div className='container'>
            <div className={styles.main1}>
                <div className={styles.Kncontainer}>
                    <div className={styles.index}>
                        <section>
                            <div>
                                <Knowledge knowledgeList={knowledgeList} />
                            </div>
                        </section>
                        <aside className={styles.sticky}>
                                <RecommendedReading recommend={recommend} classfyList={classfyList} />
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default archives