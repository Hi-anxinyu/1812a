import { Route } from "umi"

const knowledge: React.FC<Route> = (props) => {
    return <div>
        {
            props.children
        }
    </div>
}
export default knowledge