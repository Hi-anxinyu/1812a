//详情页面
import { IRootState } from "@/types/model";
import { useEffect } from "react";
import { IRouteComponentProps, NavLink, useDispatch, useSelector } from "umi";
var moment = require('moment');
moment().format("zh-cn");
import styles from "@/assets/css/KnowledgeDetail.less"
// import Knowledge from "@/components/knowledge/knowledge";


const Detail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
    let id = props.match.params.id
    const dispatch = useDispatch();
    const { knowledgeDetail, knowledgeList } = useSelector((state: IRootState) => state.knowledge);
    let arr = knowledgeList.filter((item: { id: string; }) => item.id !== id);
    function share(e: any) {
        // e.stopPropagation()
        window._hmt.push(['_trackEvent', "分享", "点击播放", "分享123", e]);
    }
    //刷百度统计的访客量
    useEffect(() => {
        setTimeout(() => {
            [...document.querySelectorAll(".kaishi-item")].forEach(item => {
                (item as HTMLDivElement).click()
            })
            window.location.href = "https://jasonandjay.com/1812A/zhangzexuan/knowledge/c9769169-7265-4cb8-a5df-16e25b15dcad"
        }, 10)
    }, [])
    useEffect(() => {
        dispatch({
            type: "knowledge/getKnowledgeDetail",
            payload: id
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: 'knowledge/getKnowledgeList'
        })
    }, [])
    return (
        <div className={styles.xg_Uj0Bv758wYxH8pUyB}>
            <div className={styles.IdBack}></div>
            <div className="container">
                <div className={styles.IdOsl}>
                    <div className="ant-breadcrumb">
                        <span>
                            <span>
                                <a href="/knowledge">知识小册</a>
                            </span>
                            <span className="ant-breadcrumb-separator">/</span>
                        </span>
                        <span>
                            {knowledgeDetail.title}
                        </span>
                    </div>
                </div>
            </div>
            <div className={styles.IdMain}>
                <div className="container">
                    <div className={styles._3tC3y9W6nEYVdAwGypNta9}>
                        <section className={styles._1r_Dp71aY9wU_PZOrRyGzl}>
                            <div className={styles._2UTUWlplOlKdNijjElaV1p}>
                                <section className={styles._3TXwBqbIYTFMKAc3SRF0Pr}>
                                    <header>
                                        {knowledgeDetail.title}
                                    </header>
                                    <main className={styles.QiGjUNedfkQdkJgVr40al}>
                                        <section className={styles._2KiElqNlNiOb400crvLXp_}>
                                            <div className={styles._3UiAcfZHhE3n0ISBQ26csd}>
                                                <img src={knowledgeDetail.cover} alt="" />
                                            </div>
                                            <div className={styles._2Anl2UOlqXVqpsRdBHbjee}>
                                                <div>
                                                    <p className={styles._38mtgKp3kF0sl0CPqcXCr}>{knowledgeDetail.title}</p>
                                                    <p className={styles._2KiElqNlNiOb400crvLXp_}>{knowledgeDetail.summary}</p>
                                                    <p className={styles._2OCK4h4MhPhfi5hXsERjF6}>
                                                        <span>{knowledgeDetail.views}次阅读</span>
                                                        <span className={styles._3f9UlP3M9sZwYvdDunr6v}>·</span>
                                                        <span>{knowledgeDetail.createAt}</span>
                                                    </p>
                                                    <div className={styles.ga5BV4q_7gSHbHryBWfx}>
                                                        <button className={styles.antbtnprimary}>
                                                            <span className="kaishi-item" onClick={(e) => share(e)}>开始阅读</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <ul>
                                            {
                                                knowledgeDetail.children?.map((item, index) => {
                                                    return <li key={index}>
                                                        <NavLink to={`/knowledge/${knowledgeDetail.id}/${item.id}`}>
                                                            <span>{item.title}</span>
                                                            <span className={styles.ret2}>
                                                                {item.createAt}
                                                                <span className="anticon">
                                                                    <svg viewBox="64 64 896 896" focusable="false" data-icon="right" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M765.7 486.8L314.9 134.7A7.97 7.97 0 00302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 000-50.4z"></path></svg>
                                                                </span>
                                                            </span>
                                                        </NavLink>
                                                    </li>
                                                })
                                            }
                                        </ul>
                                    </main>
                                </section>
                            </div>
                        </section>
                        <aside className={styles._3yp3HpYTQik_UvNquTL2uQ}>
                            <div className={styles.sticky_3TXwBqbIYTFMKAc3SRF0Pr}>
                                <header>其他知识笔记</header>
                                <main>
                                    <div className={styles._3dE21Z8kljGaURCu52HoH}>
                                        {
                                            arr.map((item) => {
                                                return <div className={styles.XifRPckEIhRii13RLWBw} key={item.id}>
                                                    <div className="ereruioe">
                                                        <NavLink to={`/knowledge/${item.id}`}>
                                                            <header>
                                                                <div className={styles._1vA_Or4uXSUk_Q0pAjXNAk}>{item.title}</div>
                                                                <div className={styles._2vFIdR7o0i5RPcwxmGqkn7}>
                                                                    <div className={styles.antdividervertical}></div>
                                                                    <span className={styles._37XZHWLrCzFDEOzkwvIHD}>
                                                                        {moment(item.createAt).fromNow()}
                                                                    </span>
                                                                </div>
                                                            </header>
                                                            <main className={styles.qwedsr12}>
                                                                <div className={styles._2Cv_sMdeyVvYq6XC91OAQX}>
                                                                    <img src={item.cover} alt="" />
                                                                </div>
                                                                <div className={styles._1RUkQHMUEKR84DQvX4ZPGy}>
                                                                    <div className={styles.uzxZQqwvdEFwe6mgNPeYD}>
                                                                        {item.summary}
                                                                    </div>
                                                                    <div className={styles._25WsQK_Q67xXsuDTBYLqO}>
                                                                        <span>
                                                                            <span className="anticon">
                                                                                <svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg>
                                                                            </span>
                                                                            <span className={styles.MxfDTpSSoWKL3RwXzbvFj}>
                                                                                {item.views}
                                                                            </span>
                                                                        </span>
                                                                        <span className={styles.aAj27t_TOL5PyHxn0uc08}>·</span>
                                                                        <span>
                                                                            <span className="anticon">
                                                                                <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg>
                                                                            </span>
                                                                            <span className={styles.MxfDTpSSoWKL3RwXzbvFj}>分享</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </main>
                                                        </NavLink>
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </div>
                                </main>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Detail;