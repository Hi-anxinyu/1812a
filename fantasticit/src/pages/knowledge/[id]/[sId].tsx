import { IRootState } from "@/types";
import { useEffect, useState } from "react";
import { IRouteComponentProps, NavLink, useDispatch, useSelector } from "umi";
import styles from "@/assets/css/KnowledgeDetailDetail.less"
import HighLight from "@/components/HeightLight";
import ImageView from "@/components/ImageView";
import RecommendedReading from "@/components/Recommended-reading";

const header: React.FC<IRouteComponentProps<{ sId: string, id: string }>> = (props) => {
    // console.log(props)
    let id = props.match.params.sId
    let id1 = props.match.params.id;
    const dispatch = useDispatch();
    const { knowledgeDetailDetail, knowledgeDetail, knowledgeList } = useSelector((state: IRootState) => state.knowledge);
    const [list, setList] = useState([{ "level": "2", "id": "1-速度是关键", "text": "1 速度是关键" }, { "level": "2", "id": "2-延迟的构成", "text": "2 延迟的构成" }, { "level": "2", "id": "3-光速与传播延迟", "text": "3 光速与传播延迟" }, { "level": "2", "id": "4-延迟的最后一公里", "text": "4 延迟的最后一公里" }, { "level": "2", "id": "5-网络核心的带宽", "text": "5 网络核心的带宽" }, { "level": "2", "id": "6-网络边缘的带宽", "text": "6 网络边缘的带宽" }, { "level": "2", "id": "7-目标：高带宽和低延迟", "text": "7 目标：高带宽和低延迟" }])
    useEffect(() => {
        dispatch({
            type: "knowledge/getKnowledgeDetailDetail",
            payload: id,
        })
    }, [])

    useEffect(() => {
        dispatch({
            type: "knowledge/getKnowledgeDetail",
            payload: id1
        })
    }, [])
    console.log(knowledgeDetail);
    console.log(knowledgeDetailDetail);


    let tocItme = knowledgeDetailDetail.toc
    console.log(tocItme);

    // let arr1=tocItme?.split(",");
    // let arr1 = JSON.parse("[" + tocItme + "]")
    // console.log(arr1);

    // console.log(knowledgeDetailDetail.toc)
    return (
        <div className={styles.SId}>
            <div className="container">
                <div className={styles._3tC3y9W6nEYVdAwGypNta9}>
                    <div>三个小图标</div>
                    <section className={styles._1r_Dp71aY9wU_PZOrRyGzl}>
                        <div className={styles._2GvneS9joInoiS8u5XXv}>
                            <div className="ant-breadcrumb">
                                <span>
                                    <span><NavLink to={`/knowledge`}>知识小册</NavLink></span>
                                    <span className={styles.antbreadcrumbseparator}>/</span>
                                </span>
                                <span>                                                                         {/*   no? */}
                                    <span><NavLink to={`/knowledge/${knowledgeDetail.id}`}>{knowledgeDetail.title}</NavLink></span>
                                    <span>/</span>
                                </span>
                                <span>
                                    <span>{knowledgeDetailDetail.title}</span>
                                    <span className={styles.antbreadcrumbseparators}>/</span>
                                </span>
                            </div>
                        </div>
                        <div className={styles._3kQ1Vl_751MJWlFiA4mkIJ}>
                            <article>
                                <div className={styles._1sHvTwlFk1qI5GYNQQYAOA}>
                                    <h1 className={styles._1gyFGCKvmuw_FCv8gpxCM}>{knowledgeDetailDetail.title}</h1>
                                    <p className={styles._3kL5VVYs3ZqoG6F75pb9FZ}>
                                        <span>发布于<span>{knowledgeDetailDetail.createAt}</span></span>
                                        <span> • </span>
                                        <span>阅读量{knowledgeDetailDetail.views}</span>
                                    </p>
                                </div>
                                {/* 数据 */}
                                <div>
                                    <HighLight>
                                        <div dangerouslySetInnerHTML={{ __html: knowledgeDetailDetail.html! }}>
                                        </div>
                                    </HighLight>
                                    {/* <ImageView>
                                        <div dangerouslySetInnerHTML={{__html:knowledgeDetailDetail.html!}}>
                                        </div>
                                    </ImageView> */}
                                </div>
                                <div className={styles.WsHfV3fGKFel9niUnWSq3}>
                                    发布于<span>{knowledgeDetailDetail.createAt}</span> | 版权信息 <a href="https://creativecommons.org/licenses/by-nc/3.0/cn/deed.zh" target="_blank" rel="noreferrer">非商用-署名-自由转载</a>
                                </div>
                                <div className={styles._32zoHrkPFOCJ0FoSxx3WN}>
                                    <div className={styles._1tv35f3f10IbnKHej4MDB}>
                                        <NavLink to={`/knowledge/${knowledgeDetailDetail.id}`}>
                                            <span>TCP的构成</span>
                                            <span className="anticon"><svg viewBox="64 64 896 896" focusable="false" data-icon="right" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M765.7 486.8L314.9 134.7A7.97 7.97 0 00302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 000-50.4z"></path></svg></span>
                                        </NavLink>
                                    </div>
                                </div>
                            </article>
                            <div className={styles._3CYEcDufMKzdQRUqbAurx3}>
                                <p className={styles._1gyFGCKvmuw_FCv8gpxCM}>评论</p>
                                <div className="js-comment-id">
                                    <div className={styles._13J4GEYHDRu5MejyviLpnU}>
                                        <div className={styles.N1BwmgIkZZbNoeNN_FRbs}>
                                            <div className={styles.TaXtx7NU2ZhkaBRHq09o}>
                                                <textarea placeholder="请输入评论内容（支持 Markdown）" className="ant-input"></textarea>
                                            </div>
                                            <footer>
                                                <span className={styles._12avHmKjidVRHTuf5Qbd4K}>
                                                    <svg viewBox="0 0 1024 1024" width="18px" height="18px"><path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor"></path></svg>
                                                    <span>表情</span>
                                                </span>
                                                <div>
                                                    <button className="ant-btn">
                                                        <span>发布</span>
                                                    </button>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                    <div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <aside className={styles._3yp3HpYTQik_UvNquTL2uQ}>
                        <div className="sticky">
                            <div className={styles._24iEkzEiSdi7FORkH9_wX}>
                                {/* no */}
                                <header>{knowledgeDetail.title}</header>
                                <main>
                                    {/* <ul>
                                        <li><NavLink to={`/knowledge/${id1}/${id}`}>{knowledgeDetail.title}</NavLink></li>
                                        <li><NavLink to={`/knowledge/${id1}/${id}`}>TCP的构成</NavLink></li>
                                        <li><NavLink to={`/knowledge/${id1}/${id}`}>UDP的构成</NavLink></li>
                                    </ul> */}
                                    {
                                        knowledgeDetail.children && knowledgeDetail.children.map((item, index) => {
                                            return <li key={index}>
                                                <NavLink to={`/knowledge/${knowledgeDetail.id}/${knowledgeDetailDetail.id}`}>{item.title}</NavLink>
                                            </li>
                                        })
                                    }
                                </main>
                            </div>
                            <div>
                                <div>
                                    <RecommendedReading tocItme={list} />
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    )
}
export default header;