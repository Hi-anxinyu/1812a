export interface IRootObject {
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: ICategory;
  tags: ICategory[];
}

export interface ICategory {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
export interface ICommentLists {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: any[];
}


export interface ISendComment {
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
}

export interface ICommentId {
  id: string;
  cover?: any;
  name: string;
  path: string;
  order: number;
  content: string;
  html: string;
  toc: string;
  status: string;
  publishAt: string;
  views: number;
  createAt: string;
  updateAt: string;
}

export interface ImageItem {
  statusCode: number;
  msg?: any;
  success: boolean;
  data: IData;
}

export interface IData {
  id: string;
  cover: string;
  name: string;
  path: string;
  order: number;
  content: string;
  html: string;
  toc: string;
  status: string;
  publishAt: string;
  views: number;
  createAt: string;
  updateAt: string;
}