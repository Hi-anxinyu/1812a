import { ArticleModelState } from "@/models/article";
import { KnowledgeModelState } from "@/models/knowledge";
import { ArchivesModelState } from "@/models/archives";
import { CommentModelState } from '@/models/comment'
import {FeModelState} from '@/models/fe'
import {BeModelState} from '@/models/be';
import {ReadingModelState} from '@/models/reading';
import {LinuxModelState} from '@/models/linux';
import {LeetcodeModelState} from '@/models/leetcode';
import {NewsModelState} from '@/models/news';
import {LanguageModelState} from '@/models/language';
import {TagModelState} from '@/models/tag';
export interface IRootState {
    article: ArticleModelState
    knowledgeList: KnowledgeModelState
    knowledge: KnowledgeModelState;
    articleClassificationList:KnowledgeModelState;
    fe:FeModelState,
    be:BeModelState,
    reading:ReadingModelState,
    linux:LinuxModelState,
    leetcode:LeetcodeModelState,
    news:NewsModelState,
    loading:{global:boolean},
    language:LanguageModelState
    knowledgeDetail: KnowledgeModelState,
    tag:TagModelState,
    
}

export interface IArchiveRoot {
    archives: ArchivesModelState
}

export interface ICommentList {
    comment: CommentModelState
}
