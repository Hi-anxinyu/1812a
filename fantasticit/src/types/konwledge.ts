export interface IKnowledgeItem {
  id: string;
  parentId?: string;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: string;
  html?: string;
  toc?: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}
export interface IArticleClassification {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
export interface IKnowledgeDetail {
  id: string;
  parentId?: string;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: string;
  html?: string;
  toc?: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  children: Child[];
}
interface Child {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}
export interface IKnowledgeDetailDetail {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}