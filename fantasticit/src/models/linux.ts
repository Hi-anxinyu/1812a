import {getCategoryList} from '@/services';
import {IArticleItem} from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
export interface LinuxModelState {
  linuxlist:IArticleItem[];
  linuxcount:number,
}
export interface LinuxModelType {
  namespace: 'linux';
  state: LinuxModelState;
  effects: {
    getCategoryList:Effect;
  };
  reducers: {
    save: Reducer<LinuxModelState>;
  };
}
const IndexModel: LinuxModelType = {
  namespace: 'linux',
  state: {
    linuxlist:[],
    linuxcount:0,
  },
  effects: {
    *getCategoryList({payload},{call,put,select}){
        let {page,key} = payload
        let linuxlist =yield select((state:IRootState)=>state.linux.linuxlist);
        let result = yield call(getCategoryList,page,key);
        if(result.statusCode===200){
            linuxlist = page ===1? result.data[0]:[...linuxlist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
                linuxlist,
                linuxcount:result.data[1],
            }
          })
        }
    }
  },  
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;