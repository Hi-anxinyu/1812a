import { getArticleClassification, getKnowledgeList, getKnowledgeDetail, getKnowledgeDetailDetail } from "@/services/module/knowledge"
import { IKnowledgeItem, IArticleClassification, IKnowledgeDetail, IKnowledgeDetailDetail } from '@/types/konwledge';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface KnowledgeModelState {
    knowledgeList: IKnowledgeItem[];
    articleClassificationList: IArticleClassification[];
    knowledgeDetail: Partial<IKnowledgeDetail>
    knowledgeDetailDetail: Partial<IKnowledgeDetailDetail>
}

export interface KnowledgeModelType {
    namespace: 'knowledge';
    state: KnowledgeModelState;
    effects: {
        getKnowledgeList: Effect;
        getArticleClassification: Effect;
        getKnowledgeDetail: Effect;
        getKnowledgeDetailDetail: Effect;
    };
    reducers: {
        save: Reducer<KnowledgeModelState>;
    };
}

const IndexModel: KnowledgeModelType = {
    namespace: 'knowledge',
    state: {
        knowledgeList: [],//知识小册列表
        articleClassificationList: [],//文章分类
        knowledgeDetail: {},//知识页面详情
        knowledgeDetailDetail: {},//知识页面详情下的详情页面
    },
    effects: {
        *getKnowledgeList({ payload }, { call, put }) {
            let result = yield call(getKnowledgeList);
            // console.log("....result", result);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgeList: result.data[0] }
                })
            }
        },
        *getArticleClassification({ payload }, { call, put }) {
            let result = yield call(getArticleClassification);
            // console.log("....result", result);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { articleClassificationList: result.data }
                })
            }
        },
        *getKnowledgeDetail({ payload }, { call, put }) {
            // console.log(payload);
            let result = yield call(getKnowledgeDetail, payload);
            console.log("....result", result);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgeDetail: result.data }
                })
            }
        },
        *getKnowledgeDetailDetail({ payload }, { call, put }) {
            // console.log(payload);
            let result = yield call(getKnowledgeDetailDetail, payload);
            // console.log("....result", result);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgeDetailDetail: result.data }
                })
            }
        },
    },
    reducers: {
        save(state, action) {
            // console.log(state, action);
            return {
                ...state,
                ...action.payload,
            };
        },
    },
};

export default IndexModel;