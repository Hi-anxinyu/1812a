/*
 * @Author: Hi.yu 
 * @Date: 2021-08-18 08:47:54 
 * @Last Modified by:   Hi, This is My file 
 * @Last Modified time: 2021-08-18 08:47:54 
 */
import {getCategoryList} from '@/services';
import {IArticleItem} from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
export interface LeetcodeModelState {
    leetcodelist:IArticleItem[];
    leetcodecount:number,
}
export interface LeetcodeModelType {
  namespace: 'leetcode';
  state: LeetcodeModelState;
  effects: {
    getCategoryList:Effect;
  };
  reducers: {
    save: Reducer<LeetcodeModelState>;
  };
}
const IndexModel: LeetcodeModelType = {
  namespace: 'leetcode',
  state: {
    leetcodelist:[],
    leetcodecount:0,
  },
  effects: {
    *getCategoryList({payload},{call,put,select}){
        let {page,key} = payload
        let leetcodelist =yield select((state:IRootState)=>state.fe.felist);
        let result = yield call(getCategoryList,page,key);
        if(result.statusCode===200){
            leetcodelist = page ===1? result.data[0]:[...leetcodelist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
                leetcodelist,
                leetcodecount:result.data[1],
            }
          })
        }
    }
  },  
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;