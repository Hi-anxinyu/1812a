import {getCategoryList} from '@/services';
import {IArticleItem} from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
export interface BeModelState {
  belist:IArticleItem[];
  becount:number,
}
export interface BeModelType {
  namespace: 'be';
  state: BeModelState;
  effects: {
    getCategoryList:Effect;
  };
  reducers: {
    save: Reducer<BeModelState>;
  };
}

const IndexModel: BeModelType = {
  namespace: 'be',
  state: {
    belist:[],
    becount:0,
  },

  effects: {
    *getCategoryList({payload},{call,put,select}){
        let {page,key} = payload
        let belist =yield select((state:IRootState)=>state.be.belist);
        let result = yield call(getCategoryList,page,key);
        if(result.statusCode===200){
          belist = page ===1? result.data[0]:[...belist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
                belist,
                becount:result.data[1],
            }
          })
        }
    }
  },  
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;