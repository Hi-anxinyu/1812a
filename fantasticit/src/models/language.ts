import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface LanguageModelState {
    locale: string;
    locales:{label:string,value:string}[]
}

export interface LanguageModelType {
  namespace: string;
  state: LanguageModelState;
  reducers: {
    save: Reducer<LanguageModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const IndexModel: LanguageModelType = {
  namespace: 'language',
  state: {
    locale: 'zh-CN',
    locales:[
        {label:"menu.language.Chinese",value:'zh-CN'},
        {label:"menu.language.Japanese",value:'ja-JP'},
        {label:"menu.language.English",value:'en-US'},
        {label:"menu.language.Korean",value:'ko-KR'}
    ]
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
}
export default IndexModel
