
import { getArchivesList, getClassifyList } from '@/services';
import { IArchiveItem, IRootClassify } from '@/types/archives';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface ArchivesModelState {
  archivesList: IArchiveItem;
  classfyList: IRootClassify[];
}

export interface ArchivesModelType {
  namespace: 'archives';
  state: ArchivesModelState;
  effects: {
    getArchivesList: Effect;
    getClassifyList: Effect;
  };
  reducers: {
    save: Reducer<ArchivesModelState>;
  };
}
const IndexModel: ArchivesModelType = {
  namespace: 'archives',
  state: {
    archivesList: ({} as IArchiveItem),
    classfyList: []
  },
  effects: {
    *getArchivesList({ payload }, { call, put }) {
      let result = yield call(getArchivesList);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { archivesList: result.data }
        })
      }
    },
    *getClassifyList({ payload }, { call, put }) {
      let result = yield call(getClassifyList);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { classfyList: result.data }
        })
      }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;