/*
 * @Author: Hi.yu 
 * @Date: 2021-08-18 08:56:44 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-20 21:28:39
 */
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
import { IArticleItem } from '@/types';
import {_getagdata} from '@/services'
export interface TagModelState {
    tagdatalist:IArticleItem[];
    tagcount:number,
}
export interface TagModelType {
  namespace: 'tag';
  state: TagModelState;
  effects: {
    _getagdata:Effect;
  };
  reducers: {
    save: Reducer<TagModelState>;
  };
}
const IndexModel: TagModelType = {
  namespace: 'tag',
  state: {
    tagdatalist:[],
    tagcount:0,
  },
  effects: {
    *_getagdata({payload},{call,put,select}){
        let {page,id} = payload
        let tagdatalist =yield select((state:IRootState)=>state.tag.tagdatalist);
        let result = yield call(_getagdata,page,id);
        if(result.statusCode===200){
            tagdatalist = page ===1? result.data[0]:[...tagdatalist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
                tagdatalist,
                tagcount:result.data[1],
            }
          })
        }
    }
  },  
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;