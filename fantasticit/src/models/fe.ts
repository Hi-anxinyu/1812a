import {getCategoryList} from '@/services';
import {IArticleItem} from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
export interface FeModelState {
  felist:IArticleItem[];
  fecount:number,
}
export interface ArticleModelType {
  namespace: 'fe';
  state: FeModelState;
  effects: {
    getCategoryList:Effect;
  };
  reducers: {
    save: Reducer<FeModelState>;
  };
}
const IndexModel: ArticleModelType = {
  namespace: 'fe',
  state: {
    felist:[],
    fecount:0,
  },
  effects: {
    *getCategoryList({payload},{call,put,select}){
        let {page,key} = payload
        let felist =yield select((state:IRootState)=>state.fe.felist);
        let result = yield call(getCategoryList,page,key);
        if(result.statusCode===200){
          felist = page ===1? result.data[0]:[...felist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
                felist,
                fecount:result.data[1],
            }
          })
        }
    }
  },  
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;