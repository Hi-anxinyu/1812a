
import {  getAsRegards, getCommentList, getCommentListTos, getSendComment } from '@/services';
import { IRootObject,ICommentLists, ISendComment, ImageItem, IData } from '@/types/comment';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {message} from 'antd'
export interface CommentModelState {
  commentList:IRootObject[],
  commentLists:ICommentLists[],
  total:number,
  sendComment:ISendComment,
  commentId:string,
  asRegards:IData
}

export interface CommentModelType {
  namespace: 'comment';
  state: CommentModelState;
  effects: {
    getCommentList:Effect;
    getSendComment: Effect;
    getCommentListTos:Effect;
    getAsRegards:Effect;
  };
  reducers: {
    save: Reducer<CommentModelState>;
  };
}

const IndexModel: CommentModelType = {
  namespace: 'comment',

  state: {
    commentList:[] ,
    commentLists:[],
    total:0,
    sendComment:{} as ISendComment,
    commentId:'',
    asRegards:{} as IData
  },

  effects: {
 
    *getCommentList({ payload }, { call, put}){
        let result = yield call(getCommentList);
        console.log("....result",result);
            if(result.statusCode === 200){
                yield put({
                type:'save',
                payload:{commentList:result.data}
            })
            }
      },
    *getSendComment({payload},{call,put}){
      let result =yield call(getSendComment,payload)
      console.log(result,',......');
      if(result.statusCode===201){
        message.success('评论完成,已提交审核')
      }else{
          message.error('参数错误')
      }
      
    },
    *getCommentListTos({payload},{call,put}){
      let result = yield call(getCommentListTos,payload);
        console.log(result,'result........');
        console.log(payload);
        if(result.statusCode === 200){
          yield put({
            type:'save',
            payload: {commentLists: result.data[0],total:result.data[1]}
          })
        }
    },
    *getAsRegards({ payload }, { call, put}){
      let result = yield call(getAsRegards,payload)
      console.log(result);
      if(result.statusCode === 200){
        yield put({
          type:'save',
          payload: {asRegards: result.data}
        })
      }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;