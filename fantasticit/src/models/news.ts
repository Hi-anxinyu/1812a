/*
 * @Author: Hi.yu 
 * @Date: 2021-08-18 08:56:44 
 * @Last Modified by: Hi, This is My file
 * @Last Modified time: 2021-08-18 09:06:33
 */
import {getCategoryList} from '@/services';
import {IArticleItem} from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
export interface NewsModelState {
    newslist:IArticleItem[];
    newscount:number,
}
export interface NewsModelType {
  namespace: 'news';
  state: NewsModelState;
  effects: {
    getCategoryList:Effect;
  };
  reducers: {
    save: Reducer<NewsModelState>;
  };
}
const IndexModel: NewsModelType = {
  namespace: 'news',
  state: {
    newslist:[],
    newscount:0,
  },
  effects: {
    *getCategoryList({payload},{call,put,select}){
        let {page,key} = payload
        let newslist =yield select((state:IRootState)=>state.news.newslist);
        let result = yield call(getCategoryList,page,key);
        if(result.statusCode===200){
            newslist = page ===1? result.data[0]:[...newslist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
                newslist,
                newscount:result.data[1],
            }
          })
        }
    }
  },  
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;