import {getCategoryList} from '@/services';
import {IArticleItem} from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
export interface ReadingModelState {
  readinglist:IArticleItem[];
  readingcount:number,
}
export interface ReadingModelType {
  namespace: 'reading';
  state: ReadingModelState;
  effects: {
    getCategoryList:Effect;
  };
  reducers: {
    save: Reducer<ReadingModelState>;
  };
}
const IndexModel: ReadingModelType = {
  namespace: 'reading',
  state: {
    readinglist:[],
    readingcount:0,
  },
  effects: {
    *getCategoryList({payload},{call,put,select}){
        let {page,key} = payload
        let readinglist =yield select((state:IRootState)=>state.fe.felist);
        let result = yield call(getCategoryList,page,key);
        if(result.statusCode===200){
        readinglist = page ===1? result.data[0]:[...readinglist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
                readinglist,
                readingcount:result.data[1],
            }
          })
        }
    }
  },  
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;