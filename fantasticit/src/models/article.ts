import { getRecommend,getArticleList,getaglist, getArticleDetail, giveALikes,getSearchData} from '@/services';
import { IArticleDetail, IArticleItem ,ITagItem, ITocItem,ISearchItem} from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {IRootState} from '@/types/model'
export interface ArticleModelState {
  recommend: IArticleItem[];
  articlelist:IArticleItem[];
  articlecount:number,
  taglist:ITagItem[],
  articleDetail: IArticleDetail;
  tocItme: ITocItem[];
  level: number;
  isLikes: number;
  searchData:ISearchItem[]
}
export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  effects: {
    getRecommend: Effect;
    getArticleList:Effect;
    getaglist:Effect,
    getArticleDetail: Effect;
    changeTabFloor: Effect;
    changeLikes: Effect;
    getSearchData:Effect
  };
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const IndexModel: ArticleModelType = {
  namespace: 'article',

  state: {
    recommend: [],
    articlelist:[],
    articlecount:0,
    taglist:[],
    articleDetail: ({} as IArticleDetail),
    tocItme: ([] as ITocItem[]),
    level: 0,
    isLikes: 0,
    searchData:[],
  },

  effects: {
    *getRecommend({ payload }, { call, put}){
        let result = yield call(getRecommend, payload);
        if (result.statusCode === 200) {
            yield put({
                type: 'save',
                payload: {recommend: result.data}
            })
        }
    },
    *getArticleList({payload},{call,put,select}){
        let articlelist =yield select((state:IRootState)=>state.article.articlelist);
        let result = yield call(getArticleList,payload);
        if(result.statusCode===200){
          articlelist = payload ===1? result.data[0]:[...articlelist,...result.data[0]];
          yield put({
            type:'save',
            payload:{
              articlelist,
              articlecount:result.data[1],
            }
          })
        }
    },
    *getaglist({payload},{call,put}){
        let resust = yield call(getaglist,payload);
        if(resust.statusCode===200){
          yield put({
            type:'save',
            payload:{
              taglist:resust.data
            }
          })
        }
    },
    *getArticleDetail({ payload }, { call, put}) {
      let result = yield call(getArticleDetail, payload)
      if(result.statusCode===200){
        yield put({
          type:'save',
          payload:{
            articleDetail: result.data,
            tocItme: JSON.parse(result.data.toc),
            isLikes: result.data.likes
          }
        })
      }
    },
    *changeTabFloor({ payload }, { call, put}) {
      yield put({
        type: 'save',
        payload:{
          level: payload
        }
      })
    },
    *changeLikes({ payload }, { call, put}) {
      let result = yield call(giveALikes, payload);
      if (result.statusCode === 200) {
        yield put({
          type:'save',
          payload:{
            isLikes: result.data.likes
          }
        })
      }
    },
    *getSearchData({payload},{call,put}){
        let res = yield call(getSearchData,payload);
        if(res.statusCode===200){
            yield put({
              type:'save',
              payload:{
                searchData:res.data
              }
            })
        }
    }
  },  
  reducers: {
    save(state, action) {
      console.log('清空值');
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;