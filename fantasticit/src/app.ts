import { RequestConfig } from 'umi';
import { createLogger } from 'redux-logger';
import { message } from 'antd';
import Nprogress from 'nprogress';
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import 'nprogress/nprogress.css';
Nprogress.configure({ showSpinner: false });
//安新宇的错误监控
Sentry.init({
  dsn: "https://26766bbb663c4846bedce1214e98880e@o974320.ingest.sentry.io/5930189",
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
});
//禁用掉线上的consoloe.log
process.env.NODE_ENV === 'production' ? (console.log = () => {}) : null;
//dev的日志配置
export const dva =
  process.env.NODE_ENV === 'production'
    ? {}
    : {
        config: {
          onAction: createLogger(),
          onError(e: Error) {
            message.error(e.message, 3);
          },
        },
      };
export function onRouteChange({ matchedRoutes }: any) {
  Nprogress.start();
  setTimeout(() => {
    Nprogress.done();
  }, 2000);
}
const baseUrl = 'https://api.blog.wipi.tech';
let showErrot = false;
export const request: RequestConfig = {
  timeout: 30000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      return {
        url: `${baseUrl}${url}`,
        options,
      };
    },
  ],
  responseInterceptors: [
    (response) => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '禁止访问',
        404: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
        if (!showErrot) {
          showErrot = true;
          message.error({
            constent: codeMaps[response.status],
            onClose: () => (showErrot = false),
          });
        }
      }
      return response;
    },
  ],
};
