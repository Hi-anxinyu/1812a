import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {},
  dva: {
    immer: true,
    hmr: true,
  },
  locale:{},
  // scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],
  publicPath: process.env.NODE_ENV === 'production' ? '/1812A/anxinyu/fantasticit/' : '/',
  base: process.env.NODE_ENV === 'production' ? '/1812A/anxinyu/fantasticit/' : '/',
  dynamicImport: {
    loading: "@/components/Import"
  },
  hash:true,
  analytics: {
    baidu: '62276121defa13f513e3c54c651faac2',
  },
});
